# Contributing

## Coding Standards

Any contribution should follow the PHP-FIG standard and configuration found in this repository for Psalm, PHPUnit, phpcpd (Copy/Paste Detector), PHPMD (PHP Mess Detector) & PHPA (PHP Assumptions).

## Git Hooks
After cloning run `git config core.hooksPath .githooks` to enable Git hooks found in the `.githooks/` directory.