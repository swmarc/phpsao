# PHPSAO - PHP Strict Array Objects

![Bugs](https://sonarcloud.io/api/project_badges/measure?project=swmarc_phpsao&metric=alert_status) ![Bugs](https://sonarcloud.io/api/project_badges/measure?project=swmarc_phpsao&metric=reliability_rating) ![Bugs](https://sonarcloud.io/api/project_badges/measure?project=swmarc_phpsao&metric=bugs) ![Bugs](https://sonarcloud.io/api/project_badges/measure?project=swmarc_phpsao&metric=security_rating) ![Bugs](https://sonarcloud.io/api/project_badges/measure?project=swmarc_phpsao&metric=vulnerabilities)

![Bugs](https://sonarcloud.io/api/project_badges/measure?project=swmarc_phpsao&metric=sqale_rating) ![Bugs](https://sonarcloud.io/api/project_badges/measure?project=swmarc_phpsao&metric=sqale_index) ![Bugs](https://sonarcloud.io/api/project_badges/measure?project=swmarc_phpsao&metric=code_smells) ![Bugs](https://sonarcloud.io/api/project_badges/measure?project=swmarc_phpsao&metric=coverage)

![Bugs](https://sonarcloud.io/api/project_badges/measure?project=swmarc_phpsao&metric=ncloc) ![Bugs](https://sonarcloud.io/api/project_badges/measure?project=swmarc_phpsao&metric=duplicated_lines_density)

