#!/bin/sh

CWD=$(pwd)

set -eu

reset
"$CWD/vendor/bin/psalm" --no-progress 2>&1 | head -n 1
