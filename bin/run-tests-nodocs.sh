#!/bin/sh

CWD=$(pwd)

set -eu

export TERM=linux
reset

mkdir -p "$CWD/build"
composer dump-autoload

"$CWD/vendor/bin/composer-require-checker"

"$CWD/vendor/bin/psalm"

"$CWD/vendor/bin/phpa" \
  "$CWD/src"

"$CWD/vendor/bin/phpunit" \
  -c "$CWD/tests/phpunit.xml" \
  --no-coverage \
  "$CWD/tests"

"$CWD/vendor/bin/phpcpd" \
  "$CWD/src"

"$CWD/vendor/bin/phpmd" \
  src \
  html \
  cleancode,codesize,controversial,design,unusedcode \
  --ignore-violations-on-exit \
  --strict \
  --suffixes php

# Outdated dependencies.
#"$CWD/vendor/bin/phpdox" \
#  -f "$CWD/doc/phpdox.xml"
