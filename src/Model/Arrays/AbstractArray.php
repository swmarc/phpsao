<?php
declare(strict_types=1);

namespace PHPSAO\Model\Arrays;

use ArrayIterator;
use Closure;
use OutOfBoundsException;
use PHPSAO\Model\Arrays\Interfaces\AbstractArrayInterface;

/**
 * Class AbstractArray
 * @package PHPSAO\Model\Arrays
 *
 * @psalm-template T
 * @template-implements AbstractArrayInterface<T>
 */
abstract class AbstractArray implements AbstractArrayInterface
{
    public const ARRAY_KEYS_SEARCH_VALUE = null;
    public const ARRAY_KEYS_STRICT = true;
    public const ARRAY_PRESERVE_KEYS = true;

    /**
     * @psalm-var array<array-key, T>
     */
    protected array $items = [];
    protected int $count = 0;

    /**
     * @psalm-param array<array-key, T> $items
     */
    final public function __construct(array $items = [])
    {
        $empty = count($items) === 0;

        if ($empty === false) {
            foreach ($items as $index => $item) {
                /**
                 * @psalm-suppress UndefinedMethod
                 */
                static::addByIndex($index, $item);
            }

            return;
        }

        $this->clear();
    }

    /**
     * {@inheritDoc}
     */
    final public function clear(): void
    {
        $this->items = [];
        $this->count = 0;
    }

    /**
     * {@inheritDoc}
     */
    final public function __toString(): string
    {
        return \Safe\json_encode($this->items);
    }

    /**
     * {@inheritDoc}
     */
    final public function addCollection(AbstractArrayInterface $collection): void
    {
        $this->addRange($collection->getArray());
    }

    /**
     * {@inheritDoc}
     */
    final public function addRange(array $items): void
    {
        foreach ($items as $index => $item) {
            /**
             * @psalm-suppress UndefinedMethod
             */
            static::addByIndex($index, $item);
        }
    }

    /**
     * {@inheritDoc}
     */
    final public function chunk(int $size, bool $preserveKeys = false): array
    {
        $arrays = [];

        foreach (\array_chunk($this->items, $size, $preserveKeys) as $array) {
            $arrays[] = $array;
        }

        return $arrays;
    }

    /**
     * {@inheritDoc}
     */
    final public function copy(): self
    {
        return clone $this;
    }

    /**
     * {@inheritDoc}
     */
    final public function filter(Closure $callback): self
    {
        /**
         * @psalm-var array<array-key, scalar|object> $subjects
         */
        $subjects = \array_filter($this->items, $callback);

        return $this->create($subjects);
    }

    /**
     * {@inheritDoc}
     */
    final public function getArray(): array
    {
        return $this->items;
    }

    /**
     * {@inheritDoc}
     */
    final public function getIterator(): ArrayIterator
    {
        return new ArrayIterator($this->items);
    }

    /**
     * {@inheritDoc}
     */
    final public function getKeys(): array
    {
        return \array_keys($this->items, self::ARRAY_KEYS_SEARCH_VALUE, self::ARRAY_KEYS_STRICT);
    }

    /**
     * {@inheritDoc}
     */
    final public function getValues(): array
    {
        return \array_values($this->items);
    }

    /**
     * {@inheritDoc}
     */
    final public function isEmpty(): bool
    {
        return $this->size() === 0;
    }

    /**
     * {@inheritDoc}
     */
    final public function size(): int
    {
        return $this->count();
    }

    /**
     * {@inheritDoc}
     */
    final public function count(): int
    {
        return \count($this->items);
    }

    /**
     * {@inheritDoc}
     */
    final public function last(): void
    {
        \end($this->items);
    }

    /**
     * {@inheritDoc}
     */
    final public function map(Closure $callback): self
    {
        /**
         * @psalm-var array<array-key, scalar|object> $subjects
         */
        $subjects = \array_map($callback, $this->items);

        return $this->create($subjects);
    }

    /**
     * {@inheritDoc}
     */
    final public function next(): void
    {
        \next($this->items);
    }

    /**
     * {@inheritDoc}
     */
    final public function previous(): void
    {
        \prev($this->items);
    }

    /**
     * {@inheritDoc}
     */
    final public function reset(): void
    {
        \reset($this->items);
    }

    /**
     * {@inheritDoc}
     */
    final public function slice(int $offset, int $length = null, bool $preserveKeys = false): self
    {
        $instance = $this->create();
        $instance->addRange(\array_slice($this->items, $offset, $length, $preserveKeys));

        return $instance;
    }

    /**
     * Adds a single item.
     *
     * @param mixed $item
     *
     * @psalm-param T $item
     */
    final protected function abstractAdd($item): void
    {
        $this->items[] = $item;
        ++$this->count;
    }

    /**
     * Adds a single item with a specific index.
     *
     * @param int|string $index
     * @param mixed $item
     *
     * @psalm-param array-key $index
     * @psalm-param T $item
     */
    final protected function abstractAddByIndex($index, $item): void
    {
        $this->items[$index] = $item;
        ++$this->count;
    }

    /**
     * Returns an item by a specific index, else false.
     *
     * @param int|string $index
     * @return mixed
     * @throws OutOfBoundsException
     *
     * @psalm-return T
     */
    final protected function abstractGet($index)
    {
        /**
         * @psalm-suppress UndefinedMethod
         */
        if (static::containsKey($index) === false) {
            throw new OutOfBoundsException('Index does not exist.');
        }

        return $this->items[$index];
    }

    /**
     * Prepends a single item to the beginning of the list.
     *
     * @param mixed $item
     *
     * @psalm-param T $item
     */
    final protected function abstractPrepend($item): void
    {
        $this->items = [$item] + $this->items;
        $this->count = $this->size();
    }

    /**
     * Prepends a single item to the beginning of the list with a specific index.
     *
     * @param int|string $index
     * @param mixed $item
     *
     * @psalm-param array-key $index
     * @psalm-param T $item
     */
    final protected function abstractPrependByIndex($index, $item): void
    {
        $this->items = [$index => $item] + $this->items;
        $this->count = $this->size();
    }

    /**
     * @return static
     *
     * @psalm-param array<array-key, T> $items
     */
    private function create(array $items = []): self
    {
        return new static($items);
    }
}
