<?php
declare(strict_types=1);

namespace PHPSAO\Model\Arrays;

use PHPSAO\Model\Arrays\Exceptions\TypeMismatchException;
use PHPSAO\Model\Arrays\Interfaces\AbstractNumInterface;
use PHPSAO\Model\Arrays\Traits\AbstractArrayValueTrait;
use PHPSAO\Model\Arrays\Traits\AbstractNumTrait;
use Safe\Exceptions\StringsException;

/**
 * Class AbstractNum
 * @package PHPSAO\Model\Arrays
 */
abstract class AbstractNum extends AbstractArray implements AbstractNumInterface
{
    use AbstractNumTrait, AbstractArrayValueTrait
    {
        contains as private abstractContains;
        getCurrent as private abstractGetCurrent;
    }

    /**
     * Compares an item from the item list and returns true on success, else false.
     *
     * @param int|float $item
     * @throws StringsException
     * @throws TypeMismatchException
     */
    final public function contains($item): bool
    {
        $this->maybeTypeMismatchException($item);

        return $this->abstractContains($item);
    }

    /**
     * Return the current element from the item list.
     *
     * @return int|float
     * @throws StringsException
     * @throws TypeMismatchException
     */
    final public function getCurrent()
    {
        /**
         * @var float|int $item
         */
        $item = $this->abstractGetCurrent();
        $this->maybeTypeMismatchException($item);

        return $item;
    }
}
