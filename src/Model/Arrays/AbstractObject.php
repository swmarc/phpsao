<?php
declare(strict_types=1);

namespace PHPSAO\Model\Arrays;

use PHPSAO\Model\Arrays\Exceptions\TypeMismatchException;
use PHPSAO\Model\Arrays\Interfaces\AbstractObjectInterface;
use Safe\Exceptions\StringsException;

/**
 * Class AbstractObject
 * @package PHPSAO\Model\Arrays
 */
abstract class AbstractObject extends AbstractArray implements AbstractObjectInterface
{
    protected static string $typeValue = '';

    /**
     * Compares an item from the item list and returns true on success, else false.
     *
     * @throws StringsException
     * @throws TypeMismatchException
     */
    final public function contains(object $item): bool
    {
        $this->maybeTypeMismatchException($item);

        return \in_array($item, $this->items, true) === true;
    }

    /**
     * Return the current element from the item list.
     */
    final public function getCurrent(): object
    {
        /**
         * @var object $item
         */
        $item = \current($this->items);

        return $item;
    }

    /**
     * @throws StringsException
     * @throws TypeMismatchException
     */
    final protected function initType(object $item): void
    {
        $className = \get_class($item);

        if (\strlen(static::$typeValue) === 0) {
            static::$typeValue = $className;
        }

        $this->maybeTypeMismatchException($item);
    }

    /**
     * @throws StringsException
     * @throws TypeMismatchException
     */
    final protected function maybeTypeMismatchException(object $item): void
    {
        $objectName = \get_class($item);

        if ($objectName === static::$typeValue) {
            return;
        }

        throw new TypeMismatchException(
            \Safe\sprintf(
                'Type mismatch. Got `%s`, but expected `%s`. ',
                $objectName,
                static::$typeValue
            )
        );
    }
}
