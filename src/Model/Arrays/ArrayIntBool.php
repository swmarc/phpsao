<?php
declare(strict_types = 1);

namespace PHPSAO\Model\Arrays;

use OutOfBoundsException;
use PHPSAO\Model\Arrays\Interfaces\ArrayBoolValueInterface;
use PHPSAO\Model\Arrays\Interfaces\ArrayIntBoolInterface;
use PHPSAO\Model\Arrays\Interfaces\ArrayIntIndexInterface;
use PHPSAO\Model\Arrays\Traits\ArrayBoolValueTrait;
use PHPSAO\Model\Arrays\Traits\ArrayIntIndexTrait;

/**
 * Class ArrayIntBool
 * @package PHPSAO\Model\Arrays
 *
 * @psalm-suppress UnusedClass
 */
final class ArrayIntBool
extends AbstractArray
implements ArrayIntIndexInterface, ArrayBoolValueInterface, ArrayIntBoolInterface
{
    use ArrayIntIndexTrait, ArrayBoolValueTrait;

    /**
     * Adds a single item.
     */
    public function add(bool $item): void
    {
        $this->abstractAdd($item);
    }

    /**
     * Adds a single item with a specific index.
     */
    public function addByIndex(int $index, bool $item): void
    {
        $this->abstractAddByIndex($index, $item);
    }

    /**
     * Returns an item by a specific index, else false.
     *
     * @throws OutOfBoundsException
     */
    public function get(int $index): bool
    {
        /**
         * @var bool $item
         */
        $item = $this->abstractGet($index);

        return $item;
    }

    /**
     * Prepends a single item to the beginning of the list.
     */
    public function prepend(bool $item): void
    {
        $this->abstractPrepend($item);
    }

    /**
     * Prepends a single item to the beginning of the list with a specific index.
     */
    public function prependByIndex(int $index, bool $item): void
    {
        $this->abstractPrependByIndex($index, $item);
    }
}
