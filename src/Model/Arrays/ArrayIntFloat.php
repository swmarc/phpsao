<?php
declare(strict_types = 1);

namespace PHPSAO\Model\Arrays;

use OutOfBoundsException;
use PHPSAO\Model\Arrays\Interfaces\ArrayFloatValueInterface;
use PHPSAO\Model\Arrays\Interfaces\ArrayIntFloatInterface;
use PHPSAO\Model\Arrays\Interfaces\ArrayIntIndexInterface;
use PHPSAO\Model\Arrays\Traits\ArrayFloatValueTrait;
use PHPSAO\Model\Arrays\Traits\ArrayIntIndexTrait;

/**
 * Class ArrayIntFloat
 * @package PHPSAO\Model\Arrays
 *
 * @psalm-suppress UnusedClass
 */
final class ArrayIntFloat
extends AbstractArray
implements ArrayIntIndexInterface, ArrayFloatValueInterface, ArrayIntFloatInterface
{
    use ArrayIntIndexTrait, ArrayFloatValueTrait;

    /**
     * Adds a single item.
     */
    public function add(float $item): void
    {
        $this->abstractAdd($item);
    }

    /**
     * Adds a single item with a specific index.
     */
    public function addByIndex(int $index, float $item): void
    {
        $this->abstractAddByIndex($index, $item);
    }

    /**
     * Returns an item by a specific index, else false.
     *
     * @throws OutOfBoundsException
     */
    public function get(int $index): float
    {
        /**
         * @var float $item
         */
        $item = $this->abstractGet($index);

        return $item;
    }

    /**
     * Prepends a single item to the beginning of the list.
     */
    public function prepend(float $item): void
    {
        $this->abstractPrepend($item);
    }

    /**
     * Prepends a single item to the beginning of the list with a specific index.
     */
    public function prependByIndex(int $index, float $item): void
    {
        $this->abstractPrependByIndex($index, $item);
    }
}
