<?php
declare(strict_types = 1);

namespace PHPSAO\Model\Arrays;

use OutOfBoundsException;
use PHPSAO\Model\Arrays\Interfaces\ArrayIntIndexInterface;
use PHPSAO\Model\Arrays\Interfaces\ArrayIntIntInterface;
use PHPSAO\Model\Arrays\Interfaces\ArrayIntValueInterface;
use PHPSAO\Model\Arrays\Traits\ArrayIntIndexTrait;
use PHPSAO\Model\Arrays\Traits\ArrayIntValueTrait;

/**
 * Class ArrayIntInt
 * @package PHPSAO\Model\Arrays
 *
 * @psalm-suppress UnusedClass
 */
final class ArrayIntInt
extends AbstractArray
implements ArrayIntIndexInterface, ArrayIntValueInterface, ArrayIntIntInterface
{
    use ArrayIntIndexTrait, ArrayIntValueTrait;

    /**
     * Adds a single item.
     */
    public function add(int $item): void
    {
        $this->abstractAdd($item);
    }

    /**
     * Adds a single item with a specific index.
     */
    public function addByIndex(int $index, int $item): void
    {
        $this->abstractAddByIndex($index, $item);
    }

    /**
     * Returns an item by a specific index, else false.
     *
     * @throws OutOfBoundsException
     */
    public function get(int $index): int
    {
        /**
         * @var int $item
         */
        $item = $this->abstractGet($index);

        return $item;
    }

    /**
     * Prepends a single item to the beginning of the list.
     */
    public function prepend(int $item): void
    {
        $this->abstractPrepend($item);
    }

    /**
     * Prepends a single item to the beginning of the list with a specific index.
     */
    public function prependByIndex(int $index, int $item): void
    {
        $this->abstractPrependByIndex($index, $item);
    }
}
