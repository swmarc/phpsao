<?php
declare(strict_types=1);

namespace PHPSAO\Model\Arrays;

use OutOfBoundsException;
use PHPSAO\Model\Arrays\Exceptions\TypeMismatchException;
use PHPSAO\Model\Arrays\Interfaces\ArrayIntIndexInterface;
use PHPSAO\Model\Arrays\Interfaces\ArrayIntNumInterface;
use PHPSAO\Model\Arrays\Traits\ArrayIntIndexTrait;
use Safe\Exceptions\StringsException;

/**
 * Class ArrayIntNum
 * @package PHPSAO\Model\Arrays
 *
 * @psalm-suppress UnusedClass
 */
final class ArrayIntNum extends AbstractNum implements ArrayIntIndexInterface, ArrayIntNumInterface
{
    use ArrayIntIndexTrait;

    /**
     * @param int|float $item
     * @throws StringsException
     * @throws TypeMismatchException
     */
    public function add($item): void
    {
        $this->initType($item);
        $this->abstractAdd($item);
    }

    /**
     * @param int|float $item
     * @throws StringsException
     * @throws TypeMismatchException
     */
    public function addByIndex(int $index, $item): void
    {
        $this->initType($item);
        $this->abstractAddByIndex($index, $item);
    }

    /**
     * @return int|float
     * @throws OutOfBoundsException
     * @throws StringsException
     * @throws TypeMismatchException
     */
    public function get(int $index)
    {
        /**
         * @var int|float $item
         */
        $item = $this->abstractGet($index);
        $this->maybeTypeMismatchException($item);

        return $item;
    }

    /**
     * @param int|float $item
     * @throws StringsException
     * @throws TypeMismatchException
     */
    public function prepend($item): void
    {
        $this->initType($item);
        $this->abstractPrepend($item);
    }

    /**
     * @param int|float $item
     * @throws StringsException
     * @throws TypeMismatchException
     */
    public function prependByIndex(int $index, $item): void
    {
        $this->initType($item);
        $this->abstractPrependByIndex($index, $item);
    }
}
