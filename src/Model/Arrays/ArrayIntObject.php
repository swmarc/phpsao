<?php
declare(strict_types=1);

namespace PHPSAO\Model\Arrays;

use OutOfBoundsException;
use PHPSAO\Model\Arrays\Exceptions\TypeMismatchException;
use PHPSAO\Model\Arrays\Interfaces\ArrayIntObjectInterface;
use PHPSAO\Model\Arrays\Traits\ArrayIntIndexTrait;
use Safe\Exceptions\StringsException;

/**
 * Class ArrayIntObject
 * @package PHPSAO\Model\Arrays
 *
 * @psalm-suppress UnusedClass
 */
final class ArrayIntObject extends AbstractObject implements ArrayIntObjectInterface
{
    use ArrayIntIndexTrait;

    /**
     * Adds a single item.
     *
     * @throws StringsException
     * @throws TypeMismatchException
     */
    public function add(object $item): void
    {
        $this->initType($item);
        $this->abstractAdd($item);
    }

    /**
     * Adds a single item with a specific index.
     *
     * @throws StringsException
     * @throws TypeMismatchException
     */
    public function addByIndex(int $index, object $item): void
    {
        $this->initType($item);
        $this->abstractAddByIndex($index, $item);
    }

    /**
     * Returns an item by a specific index, else false.
     *
     * @throws OutOfBoundsException
     * @throws StringsException
     * @throws TypeMismatchException
     */
    public function get(int $index): object
    {
        /**
         * @var object $item
         */
        $item = $this->abstractGet($index);
        $this->maybeTypeMismatchException($item);

        return $item;
    }

    /**
     * Prepends a single item to the beginning of the list.
     *
     * @throws StringsException
     * @throws TypeMismatchException
     */
    public function prepend(object $item): void
    {
        $this->initType($item);
        $this->abstractPrepend($item);
    }

    /**
     * Prepends a single item to the beginning of the list with a specific index.
     *
     * @throws StringsException
     * @throws TypeMismatchException
     */
    public function prependByIndex(int $index, object $item): void
    {
        $this->initType($item);
        $this->abstractPrependByIndex($index, $item);
    }
}
