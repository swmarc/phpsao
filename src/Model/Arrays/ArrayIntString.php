<?php
declare(strict_types = 1);

namespace PHPSAO\Model\Arrays;

use OutOfBoundsException;
use PHPSAO\Model\Arrays\Interfaces\ArrayIntIndexInterface;
use PHPSAO\Model\Arrays\Interfaces\ArrayIntStringInterface;
use PHPSAO\Model\Arrays\Interfaces\ArrayStringValueInterface;
use PHPSAO\Model\Arrays\Traits\ArrayIntIndexTrait;
use PHPSAO\Model\Arrays\Traits\ArrayStringValueTrait;

/**
 * Class ArrayIntString
 * @package PHPSAO\Model\Arrays
 *
 * @psalm-suppress UnusedClass
 */
final class ArrayIntString
extends AbstractArray
implements ArrayIntIndexInterface, ArrayStringValueInterface, ArrayIntStringInterface
{
    use ArrayIntIndexTrait, ArrayStringValueTrait;

    /**
     * Adds a single item.
     */
    public function add(string $item): void
    {
        $this->abstractAdd($item);
    }

    /**
     * Adds a single item with a specific index.
     */
    public function addByIndex(int $index, string $item): void
    {
        $this->abstractAddByIndex($index, $item);
    }

    /**
     * Returns an item by a specific index, else false.
     *
     * @throws OutOfBoundsException
     */
    public function get(int $index): string
    {
        /**
         * @var string $item
         */
        $item = $this->abstractGet($index);

        return $item;
    }

    /**
     * Prepends a single item to the beginning of the list.
     */
    public function prepend(string $item): void
    {
        $this->abstractPrepend($item);
    }

    /**
     * Prepends a single item to the beginning of the list with a specific index.
     */
    public function prependByIndex(int $index, string $item): void
    {
        $this->abstractPrependByIndex($index, $item);
    }
}
