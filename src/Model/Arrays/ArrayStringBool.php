<?php
declare(strict_types = 1);

namespace PHPSAO\Model\Arrays;

use OutOfBoundsException;
use PHPSAO\Model\Arrays\Interfaces\ArrayBoolValueInterface;
use PHPSAO\Model\Arrays\Interfaces\ArrayStringBoolInterface;
use PHPSAO\Model\Arrays\Interfaces\ArrayStringIndexInterface;
use PHPSAO\Model\Arrays\Traits\ArrayBoolValueTrait;
use PHPSAO\Model\Arrays\Traits\ArrayStringIndexTrait;

/**
 * Class ArrayStringBool
 * @package PHPSAO\Model\Arrays
 *
 * @psalm-suppress UnusedClass
 */
final class ArrayStringBool
extends AbstractArray
implements ArrayStringIndexInterface, ArrayBoolValueInterface, ArrayStringBoolInterface
{
    use ArrayStringIndexTrait, ArrayBoolValueTrait;

    /**
     * Adds a single item with a specific index.
     */
    public function addByIndex(string $index, bool $item): void
    {
        $this->abstractAddByIndex($index, $item);
    }

    /**
     * Returns an item by a specific index, else false.
     *
     * @throws OutOfBoundsException
     */
    public function get(string $index): bool
    {
        /**
         * @var bool $item
         */
        $item = $this->abstractGet($index);

        return $item;
    }

    /**
     * Prepends a single item to the beginning of the list with a specific index.
     */
    public function prependByIndex(string $index, bool $item): void
    {
        $this->abstractPrependByIndex($index, $item);
    }
}
