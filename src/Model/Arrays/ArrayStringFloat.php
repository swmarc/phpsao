<?php
declare(strict_types = 1);

namespace PHPSAO\Model\Arrays;

use OutOfBoundsException;
use PHPSAO\Model\Arrays\Interfaces\ArrayFloatValueInterface;
use PHPSAO\Model\Arrays\Interfaces\ArrayStringFloatInterface;
use PHPSAO\Model\Arrays\Interfaces\ArrayStringIndexInterface;
use PHPSAO\Model\Arrays\Traits\ArrayFloatValueTrait;
use PHPSAO\Model\Arrays\Traits\ArrayStringIndexTrait;

/**
 * Class ArrayStringFloat
 * @package PHPSAO\Model\Arrays
 *
 * @psalm-suppress UnusedClass
 */
final class ArrayStringFloat
extends AbstractArray
implements ArrayStringIndexInterface, ArrayFloatValueInterface, ArrayStringFloatInterface
{
    use ArrayStringIndexTrait, ArrayFloatValueTrait;

    /**
     * Adds a single item with a specific index.
     */
    public function addByIndex(string $index, float $item): void
    {
        $this->abstractAddByIndex($index, $item);
    }

    /**
     * Returns an item by a specific index, else false.
     *
     * @throws OutOfBoundsException
     */
    public function get(string $index): float
    {
        /**
         * @var float $item
         */
        $item = $this->abstractGet($index);

        return $item;
    }

    /**
     * Prepends a single item to the beginning of the list with a specific index.
     */
    public function prependByIndex(string $index, float $item): void
    {
        $this->abstractPrependByIndex($index, $item);
    }
}
