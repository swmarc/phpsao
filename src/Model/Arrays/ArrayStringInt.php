<?php
declare(strict_types = 1);

namespace PHPSAO\Model\Arrays;

use OutOfBoundsException;
use PHPSAO\Model\Arrays\Interfaces\ArrayIntValueInterface;
use PHPSAO\Model\Arrays\Interfaces\ArrayStringIndexInterface;
use PHPSAO\Model\Arrays\Interfaces\ArrayStringIntInterface;
use PHPSAO\Model\Arrays\Traits\ArrayIntValueTrait;
use PHPSAO\Model\Arrays\Traits\ArrayStringIndexTrait;

/**
 * Class ArrayStringInt
 * @package PHPSAO\Model\Arrays
 *
 * @psalm-suppress UnusedClass
 */
final class ArrayStringInt
extends AbstractArray
implements ArrayStringIndexInterface, ArrayIntValueInterface, ArrayStringIntInterface
{
    use ArrayStringIndexTrait, ArrayIntValueTrait;

    /**
     * Adds a single item with a specific index.
     */
    public function addByIndex(string $index, int $item): void
    {
        $this->abstractAddByIndex($index, $item);
    }

    /**
     * Returns an item by a specific index, else false.
     *
     * @throws OutOfBoundsException
     */
    public function get(string $index): int
    {
        /**
         * @var int $item
         */
        $item = $this->abstractGet($index);

        return $item;
    }

    /**
     * Prepends a single item to the beginning of the list with a specific index.
     */
    public function prependByIndex(string $index, int $item): void
    {
        $this->abstractPrependByIndex($index, $item);
    }
}
