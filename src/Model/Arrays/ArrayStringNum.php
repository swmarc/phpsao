<?php
declare(strict_types=1);

namespace PHPSAO\Model\Arrays;

use OutOfBoundsException;
use PHPSAO\Model\Arrays\Exceptions\TypeMismatchException;
use PHPSAO\Model\Arrays\Interfaces\ArrayStringIndexInterface;
use PHPSAO\Model\Arrays\Interfaces\ArrayStringNumInterface;
use PHPSAO\Model\Arrays\Traits\ArrayStringIndexTrait;
use Safe\Exceptions\StringsException;

/**
 * Class ArrayIntNum
 * @package PHPSAO\Model\Arrays
 *
 * @psalm-suppress UnusedClass
 */
final class ArrayStringNum extends AbstractNum implements ArrayStringIndexInterface, ArrayStringNumInterface
{
    use ArrayStringIndexTrait;

    /**
     * @param int|float $item
     * @throws StringsException
     * @throws TypeMismatchException
     */
    public function addByIndex(string $index, $item): void
    {
        $this->initType($item);
        $this->abstractAddByIndex($index, $item);
    }

    /**
     * @return int|float
     * @throws OutOfBoundsException
     * @throws StringsException
     * @throws TypeMismatchException
     */
    public function get(string $index)
    {
        /**
         * @var int|float $item
         */
        $item = $this->abstractGet($index);
        $this->maybeTypeMismatchException($item);

        return $item;
    }

    /**
     * @param int|float $item
     * @throws StringsException
     * @throws TypeMismatchException
     */
    public function prependByIndex(string $index, $item): void
    {
        $this->initType($item);
        $this->abstractPrependByIndex($index, $item);
    }
}
