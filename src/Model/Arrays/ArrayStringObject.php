<?php
declare(strict_types=1);

namespace PHPSAO\Model\Arrays;

use OutOfBoundsException;
use PHPSAO\Model\Arrays\Exceptions\TypeMismatchException;
use PHPSAO\Model\Arrays\Interfaces\ArrayStringObjectInterface;
use PHPSAO\Model\Arrays\Traits\ArrayStringIndexTrait;
use Safe\Exceptions\StringsException;

/**
 * Class ArrayStringObject
 * @package PHPSAO\Model\Arrays
 *
 * @psalm-suppress UnusedClass
 */
final class ArrayStringObject extends AbstractObject implements ArrayStringObjectInterface
{
    use ArrayStringIndexTrait;

    /**
     * Adds a single item with a specific index.
     *
     * @throws StringsException
     * @throws TypeMismatchException
     */
    public function addByIndex(string $index, object $item): void
    {
        $this->initType($item);
        $this->abstractAddByIndex($index, $item);
    }

    /**
     * Returns an item by a specific index, else false.
     *
     * @throws OutOfBoundsException
     * @throws StringsException
     * @throws TypeMismatchException
     */
    public function get(string $index): object
    {
        /**
         * @var object $item
         */
        $item = $this->abstractGet($index);
        $this->maybeTypeMismatchException($item);

        return $item;
    }

    /**
     * Prepends a single item to the beginning of the list with a specific index.
     *
     * @throws StringsException
     * @throws TypeMismatchException
     */
    public function prependByIndex(string $index, object $item): void
    {
        $this->initType($item);
        $this->abstractPrependByIndex($index, $item);
    }
}
