<?php
declare(strict_types = 1);

namespace PHPSAO\Model\Arrays;

use OutOfBoundsException;
use PHPSAO\Model\Arrays\Interfaces\ArrayStringIndexInterface;
use PHPSAO\Model\Arrays\Interfaces\ArrayStringStringInterface;
use PHPSAO\Model\Arrays\Interfaces\ArrayStringValueInterface;
use PHPSAO\Model\Arrays\Traits\ArrayStringIndexTrait;
use PHPSAO\Model\Arrays\Traits\ArrayStringValueTrait;

/**
 * Class ArrayStringString
 * @package PHPSAO\Model\Arrays
 *
 * @psalm-suppress UnusedClass
 */
final class ArrayStringString
extends AbstractArray
implements ArrayStringIndexInterface, ArrayStringValueInterface, ArrayStringStringInterface
{
    use ArrayStringIndexTrait, ArrayStringValueTrait;

    /**
     * Adds a single item with a specific index.
     */
    public function addByIndex(string $index, string $item): void
    {
        $this->abstractAddByIndex($index, $item);
    }

    /**
     * Returns an item by a specific index, else false.
     *
     * @throws OutOfBoundsException
     */
    public function get(string $index): string
    {
        /**
         * @var string $item
         */
        $item = $this->abstractGet($index);

        return $item;
    }

    /**
     * Prepends a single item to the beginning of the list with a specific index.
     */
    public function prependByIndex(string $index, string $item): void
    {
        $this->abstractPrependByIndex($index, $item);
    }
}
