<?php
declare(strict_types=1);

namespace PHPSAO\Model\Arrays\Exceptions;

use RuntimeException;

/**
 * Class TypeMismatchException
 * @package PHPSAO\Model\Arrays\Exceptions
 */
final class TypeMismatchException extends RuntimeException
{
}
