<?php
declare(strict_types=1);

namespace PHPSAO\Model\Arrays;

use PHPSAO\Model\Arrays\Exceptions\TypeMismatchException;
use PHPSAO\Model\Arrays\Interfaces\AbstractArrayInterface;
use PHPSAO\Model\Arrays\Interfaces\GenericInterface;
use ReflectionClass;
use ReflectionException;

/**
 * Class Generic
 * @package PHPSAO\Model\Arrays
 *
 * @psalm-suppress UnusedClass
 */
final class Generic implements GenericInterface
{
    /**
     * @param mixed ...$args
     * @return mixed
     * @throws ReflectionException
     */
    public function __invoke(...$args)
    {
        return (new ReflectionClass(self::class))
            ->getMethod('create')
            ->invokeArgs($this, $args)
        ;
    }

    /**
     * Creates an PHPSAO array object depending of the types of the key & value.
     *
     * @param string $key Possible values: string|int
     * @param string|object $value Possible values: string|int|float|bool or an object
     * @return mixed
     * @throws TypeMismatchException
     */
    public static function create(string $key, $value)
    {
        /**
         * MixedMethodCall: Cannot call constructor on an unknown class.
         * @psalm-var class-string $classPath
         */
        $classPath = self::finder($key, $value);
        if (\is_subclass_of($classPath, AbstractArrayInterface::class, true) === true) {
            return new $classPath;
        }
        /**
         * Ignore this line as Psalm forces us to check for a known class.
         * Since non-existing $key & $value values leads to an exception thrown, we never reach this line.
         */
        // @codeCoverageIgnoreStart
    }
    // @codeCoverageIgnoreEnd

    /**
     * Returns the full namespace to the PHPSAO array object.
     *
     * @param string|object $value
     * @throws TypeMismatchException
     */
    private static function finder(string $key, $value): string
    {
        $typeKey = self::getTypeKey($key);
        $typeValue = self::getTypeValue($value);

        return '\\' . __NAMESPACE__ . '\Array' . $typeKey . $typeValue;
    }

    /**
     * Maps the primitive type name from getObject() to PHPSAO primitive type name.
     *
     * @throws TypeMismatchException
     */
    private static function getTypeKey(string $type): string
    {
        $map = [
            'int' => 'Int',
            'string' => 'String',
        ];

        if (isset($map[$type]) === false) {
            throw new TypeMismatchException('Key of type `' . $type . '` is not a valid type.');
        }

        return $map[$type];
    }

    /**
     * Maps the primitive type name from getObject() to PHPSAO primitive type name.
     *
     * @param string|object $type
     * @throws TypeMismatchException
     */
    private static function getTypeValue($type): string
    {
        if (\is_object($type) === true) {
            $type = \gettype($type);
        }

        $map = [
            'bool' => 'Bool',
            'int' => 'Int',
            'float' => 'Float',
            'string' => 'String',
            'object' => 'Object',
            'num' => 'Num',
        ];

        if (isset($map[$type]) === false) {
            throw new TypeMismatchException('Value of type `' . $type . '` is not a valid type.');
        }

        return $map[$type];
    }
}
