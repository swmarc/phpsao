<?php
declare(strict_types=1);

namespace PHPSAO\Model\Arrays\Interfaces;

use ArrayIterator;
use Closure;
use Countable;
use Safe\Exceptions\JsonException;

/**
 * Interface AbstractArrayInterface
 * @package PHPSAO\Model\Arrays\Interfaces
 *
 * @psalm-template T
 */
interface AbstractArrayInterface extends Countable
{
    /**
     * @throws JsonException
     */
    public function __toString(): string;

    /**
     * Appends a subset from a Collection.
     *
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function addCollection(self $collection): void;

    /**
     * Appends a range of new items from an array.
     *
     * @psalm-param array<array-key, T> $items
     */
    public function addRange(array $items): void;

    /**
     * Split an array into chunks.
     *
     * @psalm-return array<int, array<array-key, T>>
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function chunk(int $size, bool $preserveKeys = false): array;

    /**
     * Clears the array and sets the count to zero.
     */
    public function clear(): void;

    /**
     * Returns a copy of the Array instance.
     *
     * @return static
     *
     * @psalm-return static<T>
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function copy(): self;

    /**
     *  Count all elements in an array.
     */
    public function count(): int;

    /**
     * Filters elements of an array using a callback function.
     *
     * @return static
     *
     * @psalm-param Closure(T=, array-key=):bool $callback
     * @psalm-return static<T>
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function filter(Closure $callback): self;

    /**
     * Returns the array.
     *
     * @psalm-return array<array-key, T>
     */
    public function getArray(): array;

    /**
     * Returns an array iterator.
     *
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function getIterator(): ArrayIterator;

    /**
     * Return all the keys or a subset of the keys of an array.
     *
     * @psalm-return list<array-key>
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function getKeys(): array;

    /**
     * Return all the values of an array.
     *
     * @psalm-return list<T>
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function getValues(): array;

    /**
     * Returns true or false if the array is empty.
     *
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function isEmpty(): bool;

    /**
     * Set the internal pointer of an array to its last element.
     *
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function last(): void;

    /**
     * Applies the callback to the elements of the given arrays.
     *
     * @return static
     *
     * @psalm-param Closure(T=, array-key=):bool $callback
     * @psalm-return static<T>
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function map(Closure $callback): self;

    /**
     * Advance the internal pointer of an array.
     *
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function next(): void;

    /**
     * Rewind the internal array pointer.
     *
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function previous(): void;

    /**
     * Set the internal pointer of an array to its first element.
     *
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function reset(): void;

    /**
     * Alias of count().
     *
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function size(): int;

    /**
     * Extract a slice of the array.
     *
     * @return static
     *
     * @psalm-return static<T>
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function slice(int $offset, int $length = null, bool $preserveKeys = false): self;
}
