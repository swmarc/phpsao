<?php
declare(strict_types=1);

namespace PHPSAO\Model\Arrays\Interfaces;

use PHPSAO\Model\Arrays\Exceptions\TypeMismatchException;

/**
 * Class AbstractNumInterface
 * @package PHPSAO\Model\Arrays\Interfaces
 */
interface AbstractNumInterface
{
    /**
     * @param int|float $item
     *
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function contains($item): bool;

    /**
     * @return int|float
     * @throws TypeMismatchException
     *
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function getCurrent();
}
