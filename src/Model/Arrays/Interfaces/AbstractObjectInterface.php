<?php
declare(strict_types=1);

namespace PHPSAO\Model\Arrays\Interfaces;

/**
 * Class AbstractObjectInterface
 * @package PHPSAO\Model\Arrays\Interfaces
 */
interface AbstractObjectInterface extends AbstractArrayInterface
{
    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function contains(object $item): bool;

    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function getCurrent(): object;
}
