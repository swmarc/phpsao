<?php
declare(strict_types = 1);

namespace PHPSAO\Model\Arrays\Interfaces;

/**
 * Interface ArrayBoolValueInterface
 * @package PHPSAO\Model\Arrays\Interfaces
 */
interface ArrayBoolValueInterface extends AbstractArrayInterface
{
    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function contains(bool $item): bool;

    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function getCurrent(): bool;
}
