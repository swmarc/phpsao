<?php
declare(strict_types = 1);

namespace PHPSAO\Model\Arrays\Interfaces;

/**
 * Interface ArrayFloatValueInterface
 * @package PHPSAO\Model\Arrays\Interfaces
 */
interface ArrayFloatValueInterface extends AbstractArrayInterface
{
    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function contains(float $item): bool;

    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function getCurrent(): float;
}
