<?php
declare(strict_types = 1);

namespace PHPSAO\Model\Arrays\Interfaces;

/**
 * Interface ArrayIntBoolInterface
 * @package PHPSAO\Model\Arrays\Interfaces
 */
interface ArrayIntBoolInterface extends AbstractArrayInterface
{
    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function add(bool $item): void;

    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function addByIndex(int $index, bool $item): void;

    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function get(int $index): bool;

    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function prepend(bool $item): void;

    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function prependByIndex(int $index, bool $item): void;
}
