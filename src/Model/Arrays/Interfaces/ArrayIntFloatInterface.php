<?php
declare(strict_types = 1);

namespace PHPSAO\Model\Arrays\Interfaces;

/**
 * Interface ArrayIntFloatInterface
 * @package PHPSAO\Model\Arrays\Interfaces
 */
interface ArrayIntFloatInterface extends AbstractArrayInterface
{
    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function add(float $item): void;

    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function addByIndex(int $index, float $item): void;

    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function get(int $index): float;

    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function prepend(float $item): void;

    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function prependByIndex(int $index, float $item): void;
}
