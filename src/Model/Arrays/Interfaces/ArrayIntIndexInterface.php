<?php
declare(strict_types = 1);

namespace PHPSAO\Model\Arrays\Interfaces;

/**
 * Interface ArrayIntIndexInterface
 * @package PHPSAO\Model\Arrays\Interfaces
 */
interface ArrayIntIndexInterface extends AbstractArrayInterface
{
    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function containsKey(int $index): bool;

    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function getKey(): int;

    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function reindex(): void;
}
