<?php
declare(strict_types = 1);

namespace PHPSAO\Model\Arrays\Interfaces;

/**
 * Interface ArrayIntIntInterface
 * @package PHPSAO\Model\Arrays\Interfaces
 */
interface ArrayIntIntInterface extends AbstractArrayInterface
{
    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function add(int $item): void;

    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function addByIndex(int $index, int $item): void;

    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function get(int $index): int;

    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function prepend(int $item): void;

    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function prependByIndex(int $index, int $item): void;
}
