<?php
declare(strict_types=1);

namespace PHPSAO\Model\Arrays\Interfaces;

use PHPSAO\Model\Arrays\Exceptions\TypeMismatchException;
use Safe\Exceptions\StringsException;

/**
 * Class ArrayIntNumInterface
 * @package PHPSAO\Model\Arrays\Interfaces
 */
interface ArrayIntNumInterface
{
    /**
     * @param int|float $item
     *
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function add($item): void;

    /**
     * @param int|float $item
     *
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function addByIndex(int $index, $item): void;

    /**
     * @return int|float
     *
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function get(int $index);

    /**
     * @param int|float $item
     *
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function prepend($item): void;

    /**
     * @param int|float $item
     *
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function prependByIndex(int $index, $item): void;
}
