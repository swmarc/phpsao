<?php
declare(strict_types=1);

namespace PHPSAO\Model\Arrays\Interfaces;

/**
 * Class ArrayIntObjectInterface
 * @package PHPSAO\Model\Arrays\Interfaces
 */
interface ArrayIntObjectInterface extends ArrayIntIndexInterface
{
    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function add(object $item): void;

    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function addByIndex(int $index, object $item): void;

    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function get(int $index): object;

    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function prepend(object $item): void;

    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function prependByIndex(int $index, object $item): void;
}
