<?php
declare(strict_types = 1);

namespace PHPSAO\Model\Arrays\Interfaces;

/**
 * Interface ArrayIntStringInterface
 * @package PHPSAO\Model\Arrays\Interfaces
 */
interface ArrayIntStringInterface extends AbstractArrayInterface
{
    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function add(string $item): void;

    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function addByIndex(int $index, string $item): void;

    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function get(int $index): string;

    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function prepend(string $item): void;

    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function prependByIndex(int $index, string $item): void;
}
