<?php
declare(strict_types = 1);

namespace PHPSAO\Model\Arrays\Interfaces;

/**
 * Interface ArrayIntValueInterface
 * @package PHPSAO\Model\Arrays\Interfaces
 */
interface ArrayIntValueInterface extends AbstractArrayInterface
{
    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function contains(int $item): bool;

    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function getCurrent(): int;
}
