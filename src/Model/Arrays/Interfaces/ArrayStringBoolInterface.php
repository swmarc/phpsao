<?php
declare(strict_types = 1);

namespace PHPSAO\Model\Arrays\Interfaces;

/**
 * Interface ArrayStringBoolInterface
 * @package PHPSAO\Model\Arrays\Interfaces
 */
interface ArrayStringBoolInterface extends AbstractArrayInterface
{
    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function addByIndex(string $index, bool $item): void;

    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function get(string $index): bool;

    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function prependByIndex(string $index, bool $item): void;
}
