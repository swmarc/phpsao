<?php
declare(strict_types = 1);

namespace PHPSAO\Model\Arrays\Interfaces;

/**
 * Interface ArrayStringFloatInterface
 * @package PHPSAO\Model\Arrays\Interfaces
 */
interface ArrayStringFloatInterface extends AbstractArrayInterface
{
    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function addByIndex(string $index, float $item): void;

    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function get(string $index): float;

    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function prependByIndex(string $index, float $item): void;
}
