<?php
declare(strict_types = 1);

namespace PHPSAO\Model\Arrays\Interfaces;

/**
 * Interface ArrayStringIndexInterface
 * @package PHPSAO\Model\Arrays\Interfaces
 */
interface ArrayStringIndexInterface extends AbstractArrayInterface
{
    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function containsKey(string $index): bool;

    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function getKey(): string;
}
