<?php
declare(strict_types = 1);

namespace PHPSAO\Model\Arrays\Interfaces;

/**
 * Interface ArrayStringIntInterface
 * @package PHPSAO\Model\Arrays\Interfaces
 */
interface ArrayStringIntInterface extends AbstractArrayInterface
{
    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function addByIndex(string $index, int $item): void;

    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function get(string $index): int;

    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function prependByIndex(string $index, int $item): void;
}
