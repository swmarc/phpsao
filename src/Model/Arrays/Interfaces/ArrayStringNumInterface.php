<?php
declare(strict_types=1);

namespace PHPSAO\Model\Arrays\Interfaces;

/**
 * Class ArrayIntNumInterface
 * @package PHPSAO\Model\Arrays\Interfaces
 */
interface ArrayStringNumInterface
{
    /**
     * @param int|float $item
     *
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function addByIndex(string $index, $item): void;

    /**
     * @return int|float
     *
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function get(string $index);

    /**
     * @param int|float $item
     *
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function prependByIndex(string $index, $item): void;
}
