<?php
declare(strict_types=1);

namespace PHPSAO\Model\Arrays\Interfaces;

/**
 * Class ObjectStringInterface
 * @package PHPSAO\Model\Arrays\Interfaces
 */
interface ArrayStringObjectInterface extends ArrayStringIndexInterface
{
    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function addByIndex(string $index, object $item): void;

    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function get(string $index): object;

    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function prependByIndex(string $index, object $item): void;
}
