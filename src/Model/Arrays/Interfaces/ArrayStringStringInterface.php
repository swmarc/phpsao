<?php
declare(strict_types = 1);

namespace PHPSAO\Model\Arrays\Interfaces;

/**
 * Interface ArrayStringStringInterface
 * @package PHPSAO\Model\Arrays\Interfaces
 */
interface ArrayStringStringInterface extends AbstractArrayInterface
{
    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function addByIndex(string $index, string $item): void;

    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function get(string $index): string;

    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function prependByIndex(string $index, string $item): void;
}
