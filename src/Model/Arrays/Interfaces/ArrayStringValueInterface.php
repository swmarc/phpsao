<?php
declare(strict_types = 1);

namespace PHPSAO\Model\Arrays\Interfaces;

/**
 * Interface ArrayStringValueInterface
 * @package PHPSAO\Model\Arrays\Interfaces
 */
interface ArrayStringValueInterface extends AbstractArrayInterface
{
    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function contains(string $item): bool;

    /**
     * @psalm-suppress PossiblyUnusedMethod
     */
    public function getCurrent(): string;
}
