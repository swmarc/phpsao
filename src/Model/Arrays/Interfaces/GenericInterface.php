<?php
declare(strict_types=1);

namespace PHPSAO\Model\Arrays\Interfaces;

/**
 * Class GenericInterface
 * @package PHPSAO\Model\Arrays\Interfaces
 */
interface GenericInterface
{
    /**
     * @param string|object $value
     * @return mixed
     *
     * @psalm-template T
     * @psalm-return T
     * @psalm-suppress PossiblyUnusedMethod
     */
    public static function create(string $key, $value);
}
