<?php
declare(strict_types = 1);

namespace PHPSAO\Model\Arrays\Traits;

/**
 * Trait AbstractArrayIndexTrait
 * @package PHPSAO\Model\Arrays\Traits
 */
trait AbstractArrayIndexTrait
{
    /**
     * @psalm-var array<array-key, scalar|object> $items
     */
    protected array $items = [];

    /**
     * Compares an index and returns true on success, else false.
     *
     * @param string|int $index
     */
    final public function containsKey($index): bool
    {
        return isset($this->items[$index]) === true;
    }

    /**
     * Fetch a key from the item list.
     *
     * @return string|int
     */
    final public function getKey()
    {
        return \key($this->items);
    }
}
