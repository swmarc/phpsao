<?php
declare(strict_types = 1);

namespace PHPSAO\Model\Arrays\Traits;

/**
 * Trait AbstractArrayValueTrait
 * @package PHPSAO\Model\Arrays\Traits
 */
trait AbstractArrayValueTrait
{
    /**
     * @psalm-var array<array-key, scalar|object> $items
     */
    protected array $items = [];

    /**
     * Compares an item from the item list and returns true on success, else false.
     *
     * @param mixed $item
     *
     * @psalm-param scalar|object $item
     */
    private function contains($item): bool
    {
        return \in_array($item, $this->items, true) === true;
    }

    /**
     * Return the current element from the item list.
     *
     * @return mixed
     *
     * @psalm-return scalar|object
     */
    private function getCurrent()
    {
        return \current($this->items);
    }
}
