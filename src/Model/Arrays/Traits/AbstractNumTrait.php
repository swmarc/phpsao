<?php
declare(strict_types=1);

namespace PHPSAO\Model\Arrays\Traits;

use PHPSAO\Model\Arrays\Exceptions\TypeMismatchException;
use Safe\Exceptions\StringsException;

/**
 * Class AbstractNumTrait
 * @package PHPSAO\Model\Arrays\Traits
 */
trait AbstractNumTrait
{
    protected static string $typeKey = '';
    protected static string $typeAwait = 'Num';

    /**
     * @param int|float $item
     * @throws StringsException
     * @throws TypeMismatchException
     */
    protected function initType($item): void
    {
        if (\strlen(static::$typeKey) === 0) {
            static::$typeKey = self::$typeAwait;
        }

        $this->maybeTypeMismatchException($item);
    }

    /**
     * @param int|float|string|bool|object $item
     * @throws StringsException
     * @throws TypeMismatchException
     *
     * @psalm-param scalar|object $item
     */
    protected function maybeTypeMismatchException($item): void
    {
        $typeString = '';
        if (\is_int($item) === true || \is_float($item) === true) {
            $typeString = self::$typeAwait;
        }

        if ($typeString === static::$typeKey) {
            return;
        }

        throw new TypeMismatchException(
            \Safe\sprintf(
                'Type mismatch. Got `%s`, but expected `%s`. ',
                \gettype($item),
                static::$typeAwait
            )
        );
    }
}
