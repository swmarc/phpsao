<?php
declare(strict_types = 1);

namespace PHPSAO\Model\Arrays\Traits;

/**
 * Trait ArrayBoolValueTrait
 * @package PHPSAO\Model\Arrays\Traits
 */
trait ArrayBoolValueTrait
{
    use AbstractArrayValueTrait
    {
        contains as private abstractContains;
        getCurrent as private abstractGetCurrent;
    }

    /**
     * Compares an item from the item list and returns true on success, else false.
     */
    final public function contains(bool $item): bool
    {
        return $this->abstractContains($item);
    }

    /**
     * Return the current element from the item list.
     */
    final public function getCurrent(): bool
    {
        /**
         * @var bool $item
         */
        $item = $this->abstractGetCurrent();

        return $item;
    }
}
