<?php
declare(strict_types = 1);

namespace PHPSAO\Model\Arrays\Traits;

use PHPSAO\Model\Arrays\Interfaces\ArrayIntIndexInterface;

/**
 * Trait ArrayIntIndexTrait
 * @package PHPSAO\Model\Arrays\Traits
 */
trait ArrayIntIndexTrait
{
    use AbstractArrayIndexTrait
    {
        containsKey as private abstractContainsKey;
        getKey as private abstractGetKey;
    }

    /**
     * Compares an index and returns true on success, else false.
     */
    final public function containsKey(int $index): bool
    {
        return $this->abstractContainsKey($index);
    }

    /**
     * Fetch a key from the item list.
     */
    final public function getKey(): int
    {
        return (int) $this->abstractGetKey();
    }

    /**
     * Re-indexes the array starting by 0.
     */
    final public function reindex(): void
    {
        $this->items = \array_values($this->items);
    }
}
