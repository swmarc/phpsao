<?php
declare(strict_types = 1);

namespace PHPSAO\Model\Arrays\Traits;

/**
 * Trait ArrayIntValueTrait
 * @package PHPSAO\Model\Arrays\Traits
 */
trait ArrayIntValueTrait
{
    use AbstractArrayValueTrait
    {
        contains as private abstractContains;
        getCurrent as private abstractGetCurrent;
    }

    /**
     * Compares an item from the item list and returns true on success, else false.
     */
    final public function contains(int $item): bool
    {
        return $this->abstractContains($item);
    }

    /**
     * Return the current element from the item list.
     */
    final public function getCurrent(): int
    {
        /**
         * @var int $item
         */
        $item = $this->abstractGetCurrent();

        return $item;
    }
}
