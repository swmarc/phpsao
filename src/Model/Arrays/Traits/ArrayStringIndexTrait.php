<?php
declare(strict_types = 1);

namespace PHPSAO\Model\Arrays\Traits;

/**
 * Trait ArrayStringIndexTrait
 * @package PHPSAO\Model\Arrays\Traits
 */
trait ArrayStringIndexTrait
{
    use AbstractArrayIndexTrait
    {
        containsKey as private abstractContainsKey;
        getKey as private abstractGetKey;
    }

    /**
     * Compares an index and returns true on success, else false.
     */
    final public function containsKey(string $index): bool
    {
        return $this->abstractContainsKey($index);
    }

    /**
     * Fetch a key from the item list.
     */
    final public function getKey(): string
    {
        return (string) $this->abstractGetKey();
    }
}
