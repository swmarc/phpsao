<?php
declare(strict_types = 1);

namespace PHPSAO\Tests\Core;

use ReflectionClass;
use ReflectionException;
use PHPUnit\Framework\TestCase;
use ReflectionMethod;

/**
 * Class ReflectionTestClass
 * @package PHPSAO\Tests\Core
 */
abstract class ReflectionTestClass extends TestCase
{
    /**
     * @param string $className
     * @return void
     * @throws ReflectionException
     */
    final protected function validateInterfaces(string $className): void
    {
        $i = 0;
        $interfaceMethods = [];
        $class = new ReflectionClass($className);
        $interfaces = $class->getInterfaceNames();
        $classMethods = $class->getMethods();

        foreach ($interfaces as $interfaceName) {
            $interface = new ReflectionClass($interfaceName);

            $interfaceMethods = \array_merge(
                $interfaceMethods,
                \array_map(static function (ReflectionMethod $method): string
                {
                    return $method->getName();
                }, $interface->getMethods())
            );
        }

        foreach ($classMethods as $classMethod) {
            $methodName = $classMethod->getName();

            if ($classMethod->isPublic() === false) {
                continue;
            }

            if (\strpos($methodName, '__') === 0) {
                continue;
            }

            if (\in_array($methodName, $interfaceMethods, true) === false) {
                static::assertTrue(false, "Missing method $className::$methodName() in interface(s).");
                $i++;
            }
        }

        // avoid risky tests
        if ($i === 0) {
            static::assertTrue(true);
        }
    }
}
