<?php
declare(strict_types = 1);

namespace PHPSAO\Tests\Model\Arrays;

use ArrayIterator;
use OutOfBoundsException;
use PHPSAO\Tests\Core\ReflectionTestClass;
use PHPSAO\Model\Arrays\ArrayIntBool;

/**
 * Class ArrayIntBoolTest
 * @package PHPSAO\Tests\Model\Arrays
 */
class ArrayIntBoolTest extends ReflectionTestClass
{
    private array $array = [];
    private array $arrayPrepended = [];
    private array $arrayUnordered = [];

    protected function setUp(): void
    {
        $this->array = [0 => true, 1 => false];
        $this->arrayPrepended = [2 => true] + $this->array;
        $this->arrayUnordered = [1 => false, 0 => true];
    }

    public function testToString()
    {
        static::assertSame(\json_encode($this->array), (new ArrayIntBool($this->array))->__toString());
    }

    public function testInterfaces()
    {
        parent::validateInterfaces(ArrayIntBool::class);
    }

    public function testAdd()
    {
        $instance = new ArrayIntBool;
        $instance->add(true);
        static::assertTrue($instance->get(0));
        $instance->add(false);
        static::assertFalse($instance->get(1));
    }

    public function testAddByIndex()
    {
        $instance = new ArrayIntBool;
        $instance->addByIndex(0, true);
        static::assertTrue($instance->get(0));
        $instance->addByIndex(1, false);
        static::assertFalse($instance->get(1));
    }

    public function testAddCollection()
    {
        $instance1 = new ArrayIntBool($this->array);
        $instance2 = new ArrayIntBool;
        $instance2->addCollection($instance1);
        static::assertSame($instance1->getArray(), $instance2->getArray());
    }

    public function testAddRange()
    {
        $instance = new ArrayIntBool;
        $instance->addRange($this->array);
        static::assertSame($this->array, $instance->getArray());
    }

    public function testChunk()
    {
        $instances = (new ArrayIntBool($this->array))->chunk(1, ArrayIntBool::ARRAY_PRESERVE_KEYS);
        static::assertCount(2, $instances);
    }

    public function testClear()
    {
        $instance = new ArrayIntBool($this->array);
        $instance->clear();
        static::assertSame([], $instance->getArray());
    }

    public function testContains()
    {
        $array = [0 => true];

        static::assertTrue((new ArrayIntBool($array))->contains(true));
        static::assertFalse((new ArrayIntBool($array))->contains(false));
    }

    public function testContainsKey()
    {
        static::assertTrue((new ArrayIntBool($this->array))->containsKey(1));
        static::assertFalse((new ArrayIntBool($this->array))->containsKey(3));
    }

    public function testCopy()
    {
        $instance = new ArrayIntBool($this->array);
        static::assertEquals($instance, $instance->copy());
    }

    public function testCount()
    {
        static::assertSame(2, (new ArrayIntBool($this->array))->count());
        static::assertSame(2, (new ArrayIntBool($this->array))->size());
    }

    public function testGetCurrent()
    {
        $instance = new ArrayIntBool($this->array);
        static::assertTrue($instance->getCurrent());
        $instance->next();
        static::assertFalse($instance->getCurrent());
    }

    public function testFilter()
    {
        static::assertInstanceOf(
            ArrayIntBool::class,
            (new ArrayIntBool($this->array))->filter(static function(bool $item): bool
            {
                return $item;
            })
        );
    }

    public function testGet()
    {
        $instance = new ArrayIntBool($this->array);
        static::assertTrue($instance->get(0));

        $this->expectException(OutOfBoundsException::class);
        $instance->get(-1);
    }

    public function testCurrent()
    {
        $instance = new ArrayIntBool($this->array);
        static::assertTrue($instance->getCurrent());
        $instance->next();
        static::assertFalse($instance->getCurrent());
    }

    public function testItems()
    {
        static::assertIsArray((new ArrayIntBool($this->array))->getArray());
    }

    public function testGetIterator()
    {
        static::assertInstanceOf(ArrayIterator::class, (new ArrayIntBool($this->array))->getIterator());
    }

    public function testGetKey()
    {
        $instance = new ArrayIntBool($this->array);
        static::assertSame(0, $instance->getKey());
        $instance->next();
        static::assertSame(1, $instance->getKey());
    }

    public function testGetKeys()
    {
        static::assertSame(
            \array_keys($this->array, ArrayIntBool::ARRAY_KEYS_SEARCH_VALUE, ArrayIntBool::ARRAY_KEYS_STRICT),
            (new ArrayIntBool($this->array))->getKeys()
        );
    }

    public function testGetValues()
    {
        static::assertSame(\array_values($this->array), (new ArrayIntBool($this->array))->getValues());
    }

    public function testIsEmpty()
    {
        static::assertTrue((new ArrayIntBool)->isEmpty());
    }

    public function testLast()
    {
        $instance = new ArrayIntBool($this->array);
        $instance->last();
        static::assertSame(\end($this->array), $instance->getCurrent());
    }

    public function testMap()
    {
        static::assertInstanceOf(
            ArrayIntBool::class,
            (new ArrayIntBool($this->array))->map(static function(bool $item): bool
            {
                return $item;
            })
        );
    }

    public function testNext()
    {
        $instance = new ArrayIntBool($this->array);
        $instance->next();
        static::assertSame(\next($this->array), $instance->getCurrent());
    }

    public function testPrepend()
    {
        $array = $this->array;
        \array_unshift($array, true);
        \reset($array);

        $instance = new ArrayIntBool($this->array);
        $instance->prepend(true);
        $instance->reset();
        static::assertSame(\current($array), $instance->getCurrent());
    }

    public function testPrependByIndex()
    {
        $array = $this->arrayPrepended;
        \reset($array);

        $instance = new ArrayIntBool($this->array);
        $instance->prependByIndex(2, true);
        $instance->reset();
        static::assertSame(\current($array), $instance->getCurrent());
    }

    public function testPrevious()
    {
        $instance = new ArrayIntBool($this->array);
        $instance->last();
        $instance->previous();
        static::assertSame(\current($this->array), $instance->getCurrent());
    }

    public function testReindex()
    {
        $instance = new ArrayIntBool($this->arrayUnordered);
        $instance->reindex();
        static::assertSame(
            \array_keys($this->array, ArrayIntBool::ARRAY_KEYS_SEARCH_VALUE, ArrayIntBool::ARRAY_KEYS_STRICT),
            $instance->getKeys()
        );
    }

    public function testReset()
    {
        $instance = new ArrayIntBool($this->array);
        $instance->last();
        $instance->reset();
        static::assertSame(\current($this->array), $instance->getCurrent());
    }

    public function testSize()
    {
        static::assertSame(\count($this->array), (new ArrayIntBool($this->array))->size());
    }

    public function testSlice()
    {
        $instances = (new ArrayIntBool($this->array))->slice(1, 1, ArrayIntBool::ARRAY_PRESERVE_KEYS);
        static::assertInstanceOf(ArrayIntBool::class, $instances);
        static::assertCount(1, $instances);
    }
}
