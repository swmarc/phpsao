<?php
declare(strict_types = 1);

namespace PHPSAO\Tests\Model\Arrays;

use ArrayIterator;
use OutOfBoundsException;
use PHPSAO\Tests\Core\ReflectionTestClass;
use PHPSAO\Model\Arrays\AbstractArray;
use PHPSAO\Model\Arrays\ArrayIntFloat;

class ArrayIntFloatTest extends ReflectionTestClass
{
    private array $array = [];
    private array $arrayPrepended = [];
    private array $arrayUnordered = [];

    protected function setUp(): void
    {
        $this->array = [0 => 1.1, 1 => 2.2];
        $this->arrayPrepended = [2 => 3.3] + $this->array;
        $this->arrayUnordered = [1 => 2.2, 0 => 1.1];
    }

    public function testToString()
    {
        static::assertSame(\json_encode($this->array), (new ArrayIntFloat($this->array))->__toString());
    }

    public function testInterfaces()
    {
        parent::validateInterfaces(ArrayIntFloat::class);
    }

    public function testAdd()
    {
        $instance = new ArrayIntFloat;
        $instance->add(1.1);
        static::assertSame(1.1, $instance->get(0));
        $instance->add(2.2);
        static::assertSame(2.2, $instance->get(1));
    }

    public function testAddByIndex()
    {
        $instance = new ArrayIntFloat;
        $instance->addByIndex(0, 1.1);
        static::assertSame(1.1, $instance->get(0));
        $instance->addByIndex(1, 2.2);
        static::assertSame(2.2, $instance->get(1));
    }

    public function testAddCollection()
    {
        $instance1 = new ArrayIntFloat($this->array);
        $instance2 = new ArrayIntFloat;
        $instance2->addCollection($instance1);
        static::assertSame($instance1->getArray(), $instance2->getArray());
    }

    public function testAddRange()
    {
        $instance = new ArrayIntFloat;
        $instance->addRange($this->array);
        static::assertSame($this->array, $instance->getArray());
    }

    public function testChunk()
    {
        $instances = (new ArrayIntFloat($this->array))->chunk(1, ArrayIntFloat::ARRAY_PRESERVE_KEYS);
        static::assertCount(2, $instances);
    }

    public function testClear()
    {
        $instance = new ArrayIntFloat($this->array);
        $instance->clear();
        static::assertSame([], $instance->getArray());
    }

    public function testContains()
    {
        static::assertTrue((new ArrayIntFloat($this->array))->contains(1.1));
        static::assertFalse((new ArrayIntFloat($this->array))->contains(3.3));
    }

    public function testContainsKey()
    {
        static::assertTrue((new ArrayIntFloat($this->array))->containsKey(1));
        static::assertFalse((new ArrayIntFloat($this->array))->containsKey(3));
    }

    public function testCopy()
    {
        $instance = new ArrayIntFloat($this->array);
        static::assertEquals($instance, $instance->copy());
    }

    public function testCount()
    {
        static::assertSame(2, (new ArrayIntFloat($this->array))->count());
        static::assertSame(2, (new ArrayIntFloat($this->array))->size());
    }

    public function testGetCurrent()
    {
        static::assertSame(\current($this->array), (new ArrayIntFloat($this->array))->getCurrent());
    }

    public function testFilter()
    {
        static::assertInstanceOf(
            ArrayIntFloat::class,
            (new ArrayIntFloat($this->array))->filter(static function(float $item): bool
            {
                return \is_float($item);
            })
        );
    }

    public function testGet()
    {
        $instance = new ArrayIntFloat($this->array);
        static::assertSame(1.1, $instance->get(0));

        $this->expectException(OutOfBoundsException::class);
        $instance->get(-1);
    }

    public function testCurrent()
    {
        $instance = new ArrayIntFloat($this->array);
        static::assertSame(1.1, $instance->getCurrent());
        $instance->next();
        static::assertSame(2.2, $instance->getCurrent());
    }

    public function testItems()
    {
        static::assertIsArray((new ArrayIntFloat($this->array))->getArray());
    }

    public function testGetIterator()
    {
        static::assertInstanceOf(ArrayIterator::class, (new ArrayIntFloat($this->array))->getIterator());
    }

    public function testGetKey()
    {
        $instance = new ArrayIntFloat($this->array);
        static::assertSame(0, $instance->getKey());
        $instance->next();
        static::assertSame(1, $instance->getKey());
    }

    public function testGetKeys()
    {
        static::assertSame(
            \array_keys($this->array, ArrayIntFloat::ARRAY_KEYS_SEARCH_VALUE, ArrayIntFloat::ARRAY_KEYS_STRICT),
            (new ArrayIntFloat($this->array))->getKeys()
        );
    }

    public function testGetValues()
    {
        static::assertSame(\array_values($this->array), (new ArrayIntFloat($this->array))->getValues());
    }

    public function testIsEmpty()
    {
        static::assertTrue((new ArrayIntFloat)->isEmpty());
    }

    public function testLast()
    {
        $instance = (new ArrayIntFloat($this->array));
        $instance->last();
        static::assertSame(\end($this->array), $instance->getCurrent());
    }

    public function testMap()
    {
        static::assertInstanceOf(
            ArrayIntFloat::class,
            (new ArrayIntFloat($this->array))->map(static function(float $item): float
            {
                return $item;
            })
        );
    }

    public function testNext()
    {
        $instance = new ArrayIntFloat($this->array);
        $instance->next();
        static::assertSame(\next($this->array), $instance->getCurrent());
    }

    public function testPrepend()
    {
        $array = $this->array;
        \array_unshift($array, 3.3);
        \reset($array);

        $instance = new ArrayIntFloat($this->array);
        $instance->prepend(3.3);
        $instance->reset();
        static::assertSame(\current($array), $instance->getCurrent());
    }

    public function testPrependByIndex()
    {
        $array = $this->arrayPrepended;
        \reset($array);

        $instance = new ArrayIntFloat($this->array);
        $instance->prependByIndex(2, 3.3);
        $instance->reset();
        static::assertSame(\current($array), $instance->getCurrent());
    }

    public function testPrevious()
    {
        $instance = new ArrayIntFloat($this->array);
        $instance->last();
        $instance->previous();
        static::assertSame(\current($this->array), $instance->getCurrent());
    }

    public function testReindex()
    {
        $instance = new ArrayIntFloat($this->arrayUnordered);
        $instance->reindex();
        static::assertSame(
            \array_keys($this->array, AbstractArray::ARRAY_KEYS_SEARCH_VALUE, AbstractArray::ARRAY_KEYS_STRICT),
            $instance->getKeys()
        );
    }

    public function testReset()
    {
        $instance = new ArrayIntFloat($this->array);
        $instance->last();
        $instance->reset();
        static::assertSame(\current($this->array), $instance->getCurrent());
    }

    public function testSize()
    {
        static::assertSame(\count($this->array), (new ArrayIntFloat($this->array))->size());
    }

    public function testSlice()
    {
        $instances = (new ArrayIntFloat($this->array))->slice(1, 1, ArrayIntFloat::ARRAY_PRESERVE_KEYS);
        static::assertInstanceOf(ArrayIntFloat::class, $instances);
        static::assertCount(1, $instances);
    }
}
