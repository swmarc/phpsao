<?php
declare(strict_types = 1);

namespace PHPSAO\Tests\Model\Arrays;

use ArrayIterator;
use OutOfBoundsException;
use PHPSAO\Tests\Core\ReflectionTestClass;
use PHPSAO\Model\Arrays\AbstractArray;
use PHPSAO\Model\Arrays\ArrayIntInt;

/**
 * Class ArrayIntIntTest
 * @package PHPSAO\Tests\Model\Arrays
 */
final class ArrayIntIntTest extends ReflectionTestClass
{
    private array $array = [];
    private array $arrayPrepended = [];
    private array $arrayUnordered = [];

    protected function setUp(): void
    {
        $this->array = [0 => 1, 1 => 2];
        $this->arrayPrepended = [2 => 3] + $this->array;
        $this->arrayUnordered = [1 => 2, 0 => 1];
    }

    public function testToString()
    {
        static::assertSame(\json_encode($this->array), (new ArrayIntInt($this->array))->__toString());
    }

    public function testInterfaces()
    {
        parent::validateInterfaces(ArrayIntInt::class);
    }

    public function testAdd()
    {
        $instance = new ArrayIntInt;
        $instance->add(1);
        static::assertSame(1, $instance->get(0));
    }

    public function testAddByIndex()
    {
        $instance = new ArrayIntInt;
        $instance->addByIndex(0, 1);
        static::assertSame(1, $instance->get(0));
    }

    public function testAddCollection()
    {
        $instance1 = new ArrayIntInt($this->array);
        $instance2 = new ArrayIntInt;
        $instance2->addCollection($instance1);
        static::assertSame($instance1->getArray(), $instance2->getArray());
    }

    public function testAddRange()
    {
        $instance = new ArrayIntInt;
        $instance->addRange($this->array);
        static::assertSame($this->array, $instance->getArray());
    }

    public function testChunk()
    {
        $instances = (new ArrayIntInt($this->array))->chunk(1, ArrayIntInt::ARRAY_PRESERVE_KEYS);
        static::assertCount(2, $instances);
    }

    public function testClear()
    {
        $instance = new ArrayIntInt($this->array);
        $instance->clear();
        static::assertSame([], $instance->getArray());
    }

    public function testContains()
    {
        static::assertTrue((new ArrayIntInt($this->array))->contains(1));
        static::assertFalse((new ArrayIntInt($this->array))->contains(3));
    }

    public function testContainsKey()
    {
        static::assertTrue((new ArrayIntInt($this->array))->containsKey(1));
        static::assertFalse((new ArrayIntInt($this->array))->containsKey(3));
    }

    public function testCopy()
    {
        $instance = (new ArrayIntInt($this->array));
        static::assertEquals($instance, $instance->copy());
    }

    public function testCount()
    {
        static::assertSame(2, (new ArrayIntInt($this->array))->count());
        static::assertSame(2, (new ArrayIntInt($this->array))->size());
    }

    public function testGetCurrent()
    {
        static::assertSame(\current($this->array), (new ArrayIntInt($this->array))->getCurrent());
    }

    public function testFilter()
    {
        static::assertInstanceOf(
            ArrayIntInt::class,
            (new ArrayIntInt($this->array))->filter(static function(int $item): bool
            {
                return \is_int($item);
            })
        );
    }

    public function testGet()
    {
        $instance = new ArrayIntInt($this->array);
        static::assertSame(1, $instance->get(0));

        $this->expectException(OutOfBoundsException::class);
        $instance->get(-1);
    }

    public function testCurrent()
    {
        $instance = new ArrayIntInt($this->array);
        static::assertSame(1, $instance->getCurrent());
        $instance->next();
        static::assertSame(2, $instance->getCurrent());
    }

    public function testItems()
    {
        static::assertIsArray((new ArrayIntInt($this->array))->getArray());
    }

    public function testGetIterator()
    {
        static::assertInstanceOf(ArrayIterator::class, (new ArrayIntInt($this->array))->getIterator());
    }

    public function testGetKey()
    {
        $instance = new ArrayIntInt($this->array);
        static::assertSame(0, $instance->getKey());
        $instance->next();
        static::assertSame(1, $instance->getKey());
    }

    public function testGetKeys()
    {
        static::assertSame(
            \array_keys($this->array, ArrayIntInt::ARRAY_KEYS_SEARCH_VALUE, ArrayIntInt::ARRAY_KEYS_STRICT),
            (new ArrayIntInt($this->array))->getKeys()
        );
    }

    public function testGetValues()
    {
        static::assertSame(\array_values($this->array), (new ArrayIntInt($this->array))->getValues());
    }

    public function testIsEmpty()
    {
        static::assertTrue((new ArrayIntInt)->isEmpty());
    }

    public function testLast()
    {
        $instance = new ArrayIntInt($this->array);
        $instance->last();
        static::assertSame(\end($this->array), $instance->getCurrent());
    }

    public function testMap()
    {
        static::assertInstanceOf(
            ArrayIntInt::class,
            (new ArrayIntInt($this->array))->map(static function(int $item): int
            {
                return $item;
            })
        );
    }

    public function testNext()
    {
        $instance = new ArrayIntInt($this->array);
        $instance->next();
        static::assertSame(\next($this->array), $instance->getCurrent());
    }

    public function testPrepend()
    {
        $array = $this->array;
        \array_unshift($array, 3);
        \reset($array);

        $instance = new ArrayIntInt($this->array);
        $instance->prepend(3);
        $instance->reset();
        static::assertSame(\current($array), $instance->getCurrent());
    }

    public function testPrependByIndex()
    {
        $array = $this->arrayPrepended;
        \reset($array);

        $instance = new ArrayIntInt($this->array);
        $instance->prependByIndex(2, 3);
        $instance->reset();
        static::assertSame(\current($array), $instance->getCurrent());
    }

    public function testPrevious()
    {
        $instance = new ArrayIntInt($this->array);
        $instance->last();
        $instance->previous();
        static::assertSame(\current($this->array), $instance->getCurrent());
    }

    public function testReindex()
    {
        $instance = new ArrayIntInt($this->arrayUnordered);
        $instance->reindex();
        static::assertSame(
            \array_keys($this->array, AbstractArray::ARRAY_KEYS_SEARCH_VALUE, AbstractArray::ARRAY_KEYS_STRICT),
            $instance->getKeys()
        );
    }

    public function testReset()
    {
        $instance = new ArrayIntInt($this->array);
        $instance->last();
        $instance->reset();
        static::assertSame(\current($this->array), $instance->getCurrent());
    }

    public function testSize()
    {
        static::assertSame(\count($this->array), (new ArrayIntInt($this->array))->size());
    }

    public function testSlice()
    {
        $instances = (new ArrayIntInt($this->array))->slice(1, 1, ArrayIntInt::ARRAY_PRESERVE_KEYS);
        static::assertInstanceOf(ArrayIntInt::class, $instances);
        static::assertCount(1, $instances);
    }
}
