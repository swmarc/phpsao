<?php
declare(strict_types=1);

namespace PHPSAO\Tests\Model\Arrays;

use ArrayIterator;
use OutOfBoundsException;
use PHPSAO\Model\Arrays\AbstractArray;
use PHPSAO\Model\Arrays\ArrayIntNum;
use PHPSAO\Model\Arrays\Exceptions\TypeMismatchException;
use PHPSAO\Tests\Core\ReflectionTestClass;

/**
 * Class ArrayIntNumTest
 * @package PHPSAO\Tests\Model\Arrays
 */
class ArrayIntNumTest extends ReflectionTestClass
{
    private array $arrayValid = [];
    private array $arrayInvalid = [];
    private array $arrayPrepended = [];
    private array $arrayUnordered = [];

    protected function setUp(): void
    {
        $this->arrayValid = [
            0 => 1,
            1 => 2.3,
        ];

        $this->arrayInvalid = [
            0 => '1',
            '1' => 2.3,
        ];

        $this->arrayPrepended = [2 => 3] + $this->arrayValid;
        $this->arrayUnordered = [1 => 2.3, 0 => 1];
    }

    public function testToString()
    {
        static::assertSame(\json_encode($this->arrayValid), (new ArrayIntNum($this->arrayValid))->__toString());
    }

    public function testInterfaces()
    {
        parent::validateInterfaces(ArrayIntNum::class);
    }

    public function testConstructor()
    {
        static::assertInstanceOf(ArrayIntNum::class, new ArrayIntNum($this->arrayValid));

        $this->expectException(TypeMismatchException::class);
        new ArrayIntNum($this->arrayInvalid);
    }

    public function testAdd()
    {
        $instance = new ArrayIntNum;
        $instance->add(1);
        static::assertSame(1, $instance->get(0));
    }

    public function testAddByIndex()
    {
        $instance = new ArrayIntNum;
        $instance->addByIndex(0, 1);
        static::assertSame(1, $instance->get(0));

        $instance = new ArrayIntNum;
        $instance->addByIndex(0, 2.3);
        static::assertSame(2.3, $instance->get(0));

        $this->expectException(TypeMismatchException::class);
        (new ArrayIntNum)->addByIndex(0, true);
    }

    public function testAddCollection()
    {
        $instance1 = new ArrayIntNum($this->arrayValid);
        $instance2 = new ArrayIntNum;
        $instance2->addCollection($instance1);
        static::assertSame($instance1->getArray(), $instance2->getArray());

        $this->expectException(TypeMismatchException::class);
        (new ArrayIntNum)->addCollection(new ArrayIntNum([0 => true]));
    }

    public function testAddRange()
    {
        $instance = new ArrayIntNum;
        $instance->addRange($this->arrayValid);
        static::assertSame($this->arrayValid, $instance->getArray());

        $this->expectException(TypeMismatchException::class);
        (new ArrayIntNum)->addRange([0 => true]);
    }

    public function testChunk()
    {
        $instances = (new ArrayIntNum($this->arrayValid))->chunk(1, ArrayIntNum::ARRAY_PRESERVE_KEYS);

        static::assertCount(2, $instances);
    }

    public function testClear()
    {
        $instance = new ArrayIntNum($this->arrayValid);
        $instance->clear();
        static::assertSame([], $instance->getArray());
    }

    public function testContains()
    {
        static::assertTrue((new ArrayIntNum($this->arrayValid))->contains(1));
        static::assertFalse((new ArrayIntNum($this->arrayValid))->contains(3));

        $this->expectException(TypeMismatchException::class);
        static::assertFalse((new ArrayIntNum($this->arrayValid))->contains(true));
    }

    public function testContainsKey()
    {
        static::assertTrue((new ArrayIntNum($this->arrayValid))->containsKey(1));
        static::assertFalse((new ArrayIntNum($this->arrayValid))->containsKey(3));
    }

    public function testCopy()
    {
        $instance = (new ArrayIntNum($this->arrayValid));
        static::assertEquals($instance, $instance->copy());
    }

    public function testCount()
    {
        static::assertSame(2, (new ArrayIntNum($this->arrayValid))->count());
        static::assertSame(2, (new ArrayIntNum($this->arrayValid))->size());
    }

    public function testGetCurrent()
    {
        static::assertSame(\current($this->arrayValid), (new ArrayIntNum($this->arrayValid))->getCurrent());
    }

    public function testFilter()
    {
        static::assertInstanceOf(
            ArrayIntNum::class,
            (new ArrayIntNum($this->arrayValid))->filter(function($item)
            {
                return $item;
            })
        );
    }

    public function testGet()
    {
        $instance = new ArrayIntNum($this->arrayValid);
        static::assertSame(1, $instance->get(0));

        $this->expectException(OutOfBoundsException::class);
        $instance->get(-1);
    }

    public function testCurrent()
    {
        $instance = new ArrayIntNum($this->arrayValid);
        static::assertSame(1, $instance->getCurrent());
        $instance->next();
        static::assertSame(2.3, $instance->getCurrent());
    }

    public function testItems()
    {
        static::assertIsArray((new ArrayIntNum($this->arrayValid))->getArray());
    }

    public function testGetIterator()
    {
        static::assertInstanceOf(ArrayIterator::class, (new ArrayIntNum($this->arrayValid))->getIterator());
    }

    public function testGetKey()
    {
        $instance = new ArrayIntNum($this->arrayValid);
        static::assertSame(0, $instance->getKey());
        $instance->next();
        static::assertSame(1, $instance->getKey());
    }

    public function testGetKeys()
    {
        static::assertSame(
            \array_keys($this->arrayValid, ArrayIntNum::ARRAY_KEYS_SEARCH_VALUE, ArrayIntNum::ARRAY_KEYS_STRICT),
            (new ArrayIntNum($this->arrayValid))->getKeys()
        );
    }

    public function testGetValues()
    {
        static::assertSame(\array_values($this->arrayValid), (new ArrayIntNum($this->arrayValid))->getValues());
    }

    public function testIsEmpty()
    {
        static::assertTrue((new ArrayIntNum)->isEmpty());
    }

    public function testLast()
    {
        $instance = new ArrayIntNum($this->arrayValid);
        $instance->last();
        static::assertSame(\end($this->arrayValid), $instance->getCurrent());
    }

    public function testMap()
    {
        static::assertInstanceOf(
            ArrayIntNum::class,
            (new ArrayIntNum($this->arrayValid))->map(static function($item)
            {
                return $item;
            })
        );
    }

    public function testNext()
    {
        $instance = new ArrayIntNum($this->arrayValid);
        $instance->next();
        static::assertSame(\next($this->arrayValid), $instance->getCurrent());
    }

    public function testPrepend()
    {
        $array = $this->arrayValid;
        \array_unshift($array, 3);
        \reset($array);

        $instance = new ArrayIntNum($this->arrayValid);
        $instance->prepend(3);
        $instance->reset();
        static::assertSame(\current($array), $instance->getCurrent());
    }

    public function testPrependByIndex()
    {
        $array = $this->arrayPrepended;
        \reset($array);

        $instance = new ArrayIntNum($this->arrayValid);
        $instance->prependByIndex(2, 3);
        $instance->reset();
        static::assertSame(\current($array), $instance->getCurrent());
    }

    public function testPrevious()
    {
        $instance = new ArrayIntNum($this->arrayValid);
        $instance->last();
        $instance->previous();
        static::assertSame(\current($this->arrayValid), $instance->getCurrent());
    }

    public function testReindex()
    {
        $instance = new ArrayIntNum($this->arrayUnordered);
        $instance->reindex();
        static::assertSame(
            \array_keys($this->arrayValid, AbstractArray::ARRAY_KEYS_SEARCH_VALUE, AbstractArray::ARRAY_KEYS_STRICT),
            $instance->getKeys()
        );
    }

    public function testReset()
    {
        $instance = new ArrayIntNum($this->arrayValid);
        $instance->last();
        $instance->reset();
        static::assertSame(\current($this->arrayValid), $instance->getCurrent());
    }

    public function testSize()
    {
        static::assertSame(\count($this->arrayValid), (new ArrayIntNum($this->arrayValid))->size());
    }

    public function testSlice()
    {
        $instances = (new ArrayIntNum($this->arrayValid))->slice(1, 1, ArrayIntNum::ARRAY_PRESERVE_KEYS);
        static::assertInstanceOf(ArrayIntNum::class, $instances);
        static::assertCount(1, $instances);
    }
}
