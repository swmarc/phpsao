<?php
declare(strict_types=1);

namespace PHPSAO\Tests\Model\Arrays;

use ArrayIterator;
use OutOfBoundsException;
use PHPSAO\Model\Arrays\ArrayIntObject;
use PHPSAO\Model\Arrays\Exceptions\TypeMismatchException;
use PHPSAO\Tests\Core\ReflectionTestClass;
use PHPSAO\Tests\Model\Arrays\Entity\InvalidType;
use PHPSAO\Tests\Model\Arrays\Entity\ValidType;

/**
 * Class ArrayIntObjectTest
 * @package PHPSAO\Tests\Model\Arrays
 */
class ArrayIntObjectTest extends ReflectionTestClass
{
    private array $arrayValid = [];
    private array $arrayInvalid = [];
    private array $arrayUnordered = [];

    protected function setUp(): void
    {
        $this->arrayValid = [
            0 => new ValidType,
            1 => new ValidType,
        ];

        $this->arrayInvalid = [
            0 => new ValidType,
            1 => new InvalidType,
        ];

        $this->arrayUnordered = [
            1 => new ValidType,
            0 => new ValidType,
        ];
    }

    public function testToString()
    {
        static::assertSame(\json_encode($this->arrayValid), (new ArrayIntObject($this->arrayValid))->__toString());
    }

    public function testInterfaces()
    {
        parent::validateInterfaces(ArrayIntObject::class);
    }

    public function testConstructor()
    {
        static::assertInstanceOf(ArrayIntObject::class, new ArrayIntObject($this->arrayValid));
        $this->expectException(TypeMismatchException::class);
        new ArrayIntObject($this->arrayInvalid);
    }

    public function testAdd()
    {
        $instance = new ArrayIntObject;
        $instance->add(new ValidType);
        static::assertInstanceOf(ValidType::class, $instance->get(0));

        $this->expectException(TypeMismatchException::class);
        $instance->add(new InvalidType);
    }

    public function testAddByIndex()
    {
        $instance = new ArrayIntObject;
        $instance->addByIndex(0, new ValidType);
        static::assertInstanceOf(ValidType::class, $instance->get(0));

        $this->expectException(TypeMismatchException::class);
        $instance->addByIndex(1, new InvalidType);
    }

    public function testAddCollection()
    {
        $instance1 = new ArrayIntObject($this->arrayValid);
        $instance2 = new ArrayIntObject;
        $instance2->addCollection($instance1);
        static::assertSame($instance1->getArray(), $instance2->getArray());

        $this->expectException(TypeMismatchException::class);
        (new ArrayIntObject)->addCollection(new ArrayIntObject($this->arrayInvalid));
    }

    public function testAddRange()
    {
        $instance = new ArrayIntObject;
        $instance->addRange($this->arrayValid);
        static::assertSame($this->arrayValid, $instance->getArray());

        $this->expectException(TypeMismatchException::class);
        (new ArrayIntObject)->addRange($this->arrayInvalid);
    }

    public function testChunk()
    {
        $instances = (new ArrayIntObject($this->arrayValid))->chunk(1, ArrayIntObject::ARRAY_PRESERVE_KEYS);
        static::assertCount(2, $instances);
    }

    public function testClear()
    {
        $instance = new ArrayIntObject($this->arrayValid);
        $instance->clear();
        static::assertSame([], $instance->getArray());
    }

    public function testContains()
    {
        $object = new ValidType;
        $instance = new ArrayIntObject([0 => $object]);
        static::assertTrue($instance->contains($object));
        static::assertFalse($instance->contains(new ValidType));

        $this->expectException(TypeMismatchException::class);
        $instance->contains(new InvalidType);
    }

    public function testContainsKey()
    {
        $instance = (new ArrayIntObject($this->arrayValid));
        static::assertTrue($instance->containsKey(1));
        static::assertFalse($instance->containsKey(3));
    }

    public function testCopy()
    {
        $instance = (new ArrayIntObject($this->arrayValid));
        static::assertEquals($instance, $instance->copy());
    }

    public function testCount()
    {
        static::assertSame(2, (new ArrayIntObject($this->arrayValid))->count());
        static::assertSame(2, (new ArrayIntObject($this->arrayValid))->size());
    }

    public function testGetCurrent()
    {
        static::assertSame(\current($this->arrayValid), (new ArrayIntObject($this->arrayValid))->getCurrent());
    }

    public function testFilter()
    {
        static::assertInstanceOf(
            ArrayIntObject::class,
            (new ArrayIntObject($this->arrayValid))->filter(static function($item)
            {
                return $item;
            })
        );
    }

    public function testGet()
    {
        $object = new ValidType;
        $instance = new ArrayIntObject([0 => $object]);
        static::assertSame($object, $instance->get(0));

        $this->expectException(OutOfBoundsException::class);
        $instance->get(-1);
    }

    public function testCurrent()
    {
        $object1 = new ValidType;
        $object2 = new ValidType;
        $instance = new ArrayIntObject([0 => $object1, 1 => $object2]);
        static::assertSame($object1, $instance->getCurrent());
        $instance->next();
        static::assertSame($object2, $instance->getCurrent());
    }

    public function testItems()
    {
        static::assertIsArray((new ArrayIntObject($this->arrayValid))->getArray());
    }

    public function testGetIterator()
    {
        static::assertInstanceOf(ArrayIterator::class, (new ArrayIntObject($this->arrayValid))->getIterator());
    }

    public function testGetKey()
    {
        $instance = new ArrayIntObject($this->arrayValid);
        static::assertSame(0, $instance->getKey());
        $instance->next();
        static::assertSame(1, $instance->getKey());
    }

    public function testGetKeys()
    {
        static::assertSame(
            \array_keys($this->arrayValid, ArrayIntObject::ARRAY_KEYS_SEARCH_VALUE, ArrayIntObject::ARRAY_KEYS_STRICT),
            (new ArrayIntObject($this->arrayValid))->getKeys()
        );
    }

    public function testGetValues()
    {
        static::assertSame(\array_values($this->arrayValid), (new ArrayIntObject($this->arrayValid))->getValues());
    }

    public function testIsEmpty()
    {
        static::assertTrue((new ArrayIntObject)->isEmpty());
    }

    public function testLast()
    {
        $instance = new ArrayIntObject($this->arrayValid);
        $instance->last();
        static::assertSame(\end($this->arrayValid), $instance->getCurrent());
    }

    public function testMap()
    {
        static::assertInstanceOf(
            ArrayIntObject::class,
            (new ArrayIntObject($this->arrayValid))->map(static function($item)
            {
                return $item;
            })
        );
    }

    public function testNext()
    {
        $instance = new ArrayIntObject($this->arrayValid);
        $instance->next();
        static::assertSame(\next($this->arrayValid), $instance->getCurrent());
    }

    public function testPrepend()
    {
        $instance = new ArrayIntObject($this->arrayValid);
        $instance->prepend(new ValidType);
        $instance->reset();
        static::assertInstanceOf(ValidType::class, $instance->getCurrent());

        $this->expectException(TypeMismatchException::class);
        $instance->prepend(new InvalidType);
    }

    public function testPrependByIndex()
    {
        $instance = new ArrayIntObject($this->arrayValid);
        $instance->prependByIndex(2, new ValidType);
        $instance->reset();
        static::assertInstanceOf(ValidType::class, $instance->getCurrent());

        $this->expectException(TypeMismatchException::class);
        (new ArrayIntObject($this->arrayValid))->prependByIndex(2, new InvalidType);
    }

    public function testPrevious()
    {
        $instance = new ArrayIntObject($this->arrayValid);
        $instance->last();
        $instance->previous();
        static::assertSame(\current($this->arrayValid), $instance->getCurrent());
    }

    public function testReindex()
    {
        $instance = new ArrayIntObject($this->arrayUnordered);
        $instance->reindex();
        static::assertSame(
            \array_keys($this->arrayValid, ArrayIntObject::ARRAY_KEYS_SEARCH_VALUE, ArrayIntObject::ARRAY_KEYS_STRICT),
            $instance->getKeys()
        );
    }

    public function testReset()
    {
        $instance = new ArrayIntObject($this->arrayValid);
        $instance->last();
        $instance->reset();
        static::assertSame(\current($this->arrayValid), $instance->getCurrent());
    }

    public function testSize()
    {
        static::assertSame(\count($this->arrayValid), (new ArrayIntObject($this->arrayValid))->size());
    }

    public function testSlice()
    {
        $instances = (new ArrayIntObject($this->arrayValid))->slice(1, 1, ArrayIntObject::ARRAY_PRESERVE_KEYS);
        static::assertInstanceOf(ArrayIntObject::class, $instances);
        static::assertCount(1, $instances);
    }
}
