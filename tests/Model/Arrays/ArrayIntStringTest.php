<?php
declare(strict_types = 1);

namespace PHPSAO\Tests\Model\Arrays;

use ArrayIterator;
use OutOfBoundsException;
use PHPSAO\Tests\Core\ReflectionTestClass;
use PHPSAO\Model\Arrays\ArrayIntString;

/**
 * Class ArrayIntStringTest
 * @package PHPSAO\Tests\Model\Arrays
 */
class ArrayIntStringTest extends ReflectionTestClass
{
    private array $array = [];
    private array $arrayPrepended = [];
    private array $arrayUnordered = [];

    protected function setUp(): void
    {
        $this->array = [0 => '1', 1 => '2'];
        $this->arrayPrepended = [2 => '3'] + $this->array;
        $this->arrayUnordered = [1 => '2', 0 => '1'];
    }

    public function testToString()
    {
        static::assertSame(\json_encode($this->array), (new ArrayIntString($this->array))->__toString());
    }

    public function testInterfaces()
    {
        parent::validateInterfaces(ArrayIntString::class);
    }

    public function testAdd()
    {
        $instance = new ArrayIntString;
        $instance->add('1');
        static::assertSame('1', $instance->get(0));
    }

    public function testAddByIndex()
    {
        $instance = new ArrayIntString;
        $instance->addByIndex(0, '1');
        static::assertSame('1', $instance->get(0));
    }

    public function testAddCollection()
    {
        $instance1 = new ArrayIntString($this->array);
        $instance2 = new ArrayIntString;
        $instance2->addCollection($instance1);
        static::assertSame($instance1->getArray(), $instance2->getArray());
    }

    public function testAddRange()
    {
        $instance = new ArrayIntString;
        $instance->addRange($this->array);
        static::assertSame($this->array, $instance->getArray());
    }

    public function testChunk()
    {
        $instances = (new ArrayIntString($this->array))->chunk(1, ArrayIntString::ARRAY_PRESERVE_KEYS);
        static::assertCount(2, $instances);
    }

    public function testClear()
    {
        $instance = new ArrayIntString($this->array);
        $instance->clear();
        static::assertSame([], $instance->getArray());
    }

    public function testContains()
    {
        static::assertTrue((new ArrayIntString($this->array))->contains('1'));
        static::assertFalse((new ArrayIntString($this->array))->contains('3'));
    }

    public function testContainsKey()
    {
        static::assertTrue((new ArrayIntString($this->array))->containsKey(1));
        static::assertFalse((new ArrayIntString($this->array))->containsKey(3));
    }

    public function testCount()
    {
        static::assertSame(2, (new ArrayIntString($this->array))->count());
        static::assertSame(2, (new ArrayIntString($this->array))->size());
    }

    public function testCopy()
    {
        $instance = (new ArrayIntString($this->array));
        static::assertEquals($instance, $instance->copy());
    }

    public function testGetCurrent()
    {
        static::assertSame(\current($this->array), (new ArrayIntString($this->array))->getCurrent());
    }

    public function testFilter()
    {
        static::assertInstanceOf(
            ArrayIntString::class,
            (new ArrayIntString($this->array))->filter(static function(string $item): bool
            {
                return \is_string($item);
            })
        );
    }

    public function testGet()
    {
        $instance = new ArrayIntString($this->array);
        static::assertSame('1', $instance->get(0));

        $this->expectException(OutOfBoundsException::class);
        $instance->get(-1);
    }

    public function testCurrent()
    {
        $instance = new ArrayIntString($this->array);
        static::assertSame('1', $instance->getCurrent());
        $instance->next();
        static::assertSame('2', $instance->getCurrent());
    }

    public function testItems()
    {
        static::assertIsArray((new ArrayIntString($this->array))->getArray());
    }

    public function testGetIterator()
    {
        static::assertInstanceOf(ArrayIterator::class, (new ArrayIntString($this->array))->getIterator());
    }

    public function testGetKey()
    {
        $instance = new ArrayIntString($this->array);
        static::assertSame(0, $instance->getKey());
        $instance->next();
        static::assertSame(1, $instance->getKey());
    }

    public function testGetKeys()
    {
        static::assertSame(
            \array_keys($this->array, ArrayIntString::ARRAY_KEYS_SEARCH_VALUE, ArrayIntString::ARRAY_KEYS_STRICT),
            (new ArrayIntString($this->array))->getKeys()
        );
    }

    public function testGetValues()
    {
        static::assertSame(\array_values($this->array), (new ArrayIntString($this->array))->getValues());
    }

    public function testIsEmpty()
    {
        static::assertTrue((new ArrayIntString)->isEmpty());
    }

    public function testLast()
    {
        $instance = new ArrayIntString($this->array);
        $instance->last();
        static::assertSame(\end($this->array), $instance->getCurrent());
    }

    public function testMap()
    {
        static::assertInstanceOf(
            ArrayIntString::class,
            (new ArrayIntString($this->array))->map(static function(string $item): string
            {
                return $item;
            })
        );
    }

    public function testNext()
    {
        $instance = new ArrayIntString($this->array);
        $instance->next();
        static::assertSame(\next($this->array), $instance->getCurrent());
    }

    public function testPrepend()
    {
        $array = $this->array;
        \array_unshift($array, '1');
        \reset($array);

        $instance = new ArrayIntString($this->array);
        $instance->prepend('1');
        $instance->reset();
        static::assertSame(\current($array), $instance->getCurrent());
    }

    public function testPrependByIndex()
    {
        $array = $this->arrayPrepended;
        \reset($array);

        $instance = new ArrayIntString($this->array);
        $instance->prependByIndex(2, '3');
        $instance->reset();
        static::assertSame(\current($array), $instance->getCurrent());
    }

    public function testPrevious()
    {
        $instance = new ArrayIntString($this->array);
        $instance->last();
        $instance->previous();
        static::assertSame(\current($this->array), $instance->getCurrent());
    }

    public function testReindex()
    {
        $instance = new ArrayIntString($this->arrayUnordered);
        $instance->reindex();
        static::assertSame(
            \array_keys($this->array, ArrayIntString::ARRAY_KEYS_SEARCH_VALUE, ArrayIntString::ARRAY_KEYS_STRICT),
            $instance->getKeys()
        );
    }

    public function testReset()
    {
        $instance = new ArrayIntString($this->array);
        $instance->last();
        $instance->reset();
        static::assertSame(\current($this->array), $instance->getCurrent());
    }

    public function testSize()
    {
        static::assertSame(\count($this->array), (new ArrayIntString($this->array))->size());
    }

    public function testSlice()
    {
        $instances = (new ArrayIntString($this->array))->slice(1, 1, ArrayIntString::ARRAY_PRESERVE_KEYS);
        static::assertInstanceOf(ArrayIntString::class, $instances);
        static::assertCount(1, $instances);
    }
}
