<?php
declare(strict_types = 1);

namespace PHPSAO\Tests\Model\Arrays;

use ArrayIterator;
use OutOfBoundsException;
use PHPSAO\Tests\Core\ReflectionTestClass;
use PHPSAO\Model\Arrays\ArrayStringBool;

/**
 * Class ArrayStringBoolTest
 * @package PHPSAO\Tests\Model\Arrays
 */
class ArrayStringBoolTest extends ReflectionTestClass
{
    private array $array = [];
    private array $arrayPrepended = [];
    private array $arrayUnordered = [];

    protected function setUp(): void
    {
        $this->array = ['index0' => true, 'index1' => false];
        $this->arrayPrepended = ['index2' => true] + $this->array;
        $this->arrayUnordered = ['index1' => false, 'index0' => true];
    }

    public function testToString()
    {
        static::assertSame(\json_encode($this->array), (new ArrayStringBool($this->array))->__toString());
    }

    public function testInterfaces()
    {
        parent::validateInterfaces(ArrayStringBool::class);
    }

    public function testAddByIndex()
    {
        $instance = new ArrayStringBool;
        $instance->addByIndex('0', true);
        static::assertTrue($instance->get('0'));
        $instance->addByIndex('1', false);
        static::assertFalse($instance->get('1'));
    }

    public function testAddCollection()
    {
        $instance1 = new ArrayStringBool($this->array);
        $instance2 = new ArrayStringBool;
        $instance2->addCollection($instance1);
        static::assertSame($instance1->getArray(), $instance2->getArray());
    }

    public function testAddRange()
    {
        $instance = new ArrayStringBool;
        $instance->addRange($this->array);
        static::assertSame($this->array, $instance->getArray());
    }

    public function testChunk()
    {
        $instances = (new ArrayStringBool($this->array))->chunk(1, ArrayStringBool::ARRAY_PRESERVE_KEYS);
        static::assertCount(2, $instances);
    }

    public function testClear()
    {
        $instance = new ArrayStringBool($this->array);
        $instance->clear();
        static::assertSame([], $instance->getArray());
    }

    public function testContains()
    {
        $array = ['index0' => true];
        static::assertTrue((new ArrayStringBool($array))->contains(true));
        static::assertFalse((new ArrayStringBool($array))->contains(false));
    }

    public function testContainsKey()
    {
        static::assertTrue((new ArrayStringBool($this->array))->containsKey('index1'));
        static::assertFalse((new ArrayStringBool($this->array))->containsKey('index3'));
    }

    public function testCopy()
    {
        $instance = (new ArrayStringBool($this->array));
        static::assertEquals($instance, $instance->copy());
    }

    public function testCount()
    {
        static::assertSame(2, (new ArrayStringBool($this->array))->count());
        static::assertSame(2, (new ArrayStringBool($this->array))->size());
    }

    public function testGetCurrent()
    {
        static::assertSame(\current($this->array), (new ArrayStringBool($this->array))->getCurrent());
    }

    public function testFilter()
    {
        static::assertInstanceOf(
            ArrayStringBool::class,
            (new ArrayStringBool($this->array))->filter(static function(bool $item): bool
            {
                return $item;
            })
        );
    }

    public function testGet()
    {
        $instance = new ArrayStringBool($this->array);
        static::assertTrue($instance->get('index0'));

        $this->expectException(OutOfBoundsException::class);
        $instance->get('index-1');
    }

    public function testCurrent()
    {
        $instance = new ArrayStringBool($this->array);
        static::assertTrue($instance->getCurrent());
        $instance->next();
        static::assertFalse($instance->getCurrent());
    }

    public function testItems()
    {
        static::assertIsArray((new ArrayStringBool($this->array))->getArray());
    }

    public function testGetIterator()
    {
        static::assertInstanceOf(ArrayIterator::class, (new ArrayStringBool($this->array))->getIterator());
    }

    public function testGetKey()
    {
        $instance = new ArrayStringBool($this->array);
        static::assertSame('index0', $instance->getKey());
        $instance->next();
        static::assertSame('index1', $instance->getKey());
    }

    public function testGetKeys()
    {
        static::assertSame(
            \array_keys($this->array, ArrayStringBool::ARRAY_KEYS_SEARCH_VALUE, ArrayStringBool::ARRAY_KEYS_STRICT),
            (new ArrayStringBool($this->array))->getKeys()
        );
    }

    public function testGetValues()
    {
        static::assertSame(\array_values($this->array), (new ArrayStringBool($this->array))->getValues());
    }

    public function testIsEmpty()
    {
        static::assertTrue((new ArrayStringBool)->isEmpty());
    }

    public function testLast()
    {
        $instance = new ArrayStringBool($this->array);
        $instance->last();
        static::assertSame(\end($this->array), $instance->getCurrent());
    }

    public function testMap()
    {
        static::assertInstanceOf(
            ArrayStringBool::class,
            (new ArrayStringBool($this->array))->map(static function(bool $item): bool
            {
                return $item;
            })
        );
    }

    public function testNext()
    {
        $instance = new ArrayStringBool($this->array);
        $instance->next();
        static::assertSame(\next($this->array), $instance->getCurrent());
    }

    public function testPrependByIndex()
    {
        $array = $this->arrayPrepended;
        \reset($array);

        $instance = new ArrayStringBool($this->array);
        $instance->prependByIndex('2', true);
        $instance->reset();
        static::assertSame(\current($array), $instance->getCurrent());
    }

    public function testPrevious()
    {
        $instance = new ArrayStringBool($this->array);
        $instance->last();
        $instance->previous();
        static::assertSame(\current($this->array), $instance->getCurrent());
    }

    public function testReset()
    {
        $instance = new ArrayStringBool($this->array);
        $instance->last();
        $instance->reset();
        static::assertSame(\current($this->array), $instance->getCurrent());
    }

    public function testSize()
    {
        static::assertSame(\count($this->array), (new ArrayStringBool($this->array))->size());
    }

    public function testSlice()
    {
        $instances = (new ArrayStringBool($this->array))->slice(1, 1, ArrayStringBool::ARRAY_PRESERVE_KEYS);
        static::assertInstanceOf(ArrayStringBool::class, $instances);
        static::assertCount(1, $instances);
    }
}
