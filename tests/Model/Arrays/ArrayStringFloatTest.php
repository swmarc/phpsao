<?php
declare(strict_types = 1);

namespace PHPSAO\Tests\Model\Arrays;

use ArrayIterator;
use OutOfBoundsException;
use PHPSAO\Tests\Core\ReflectionTestClass;
use PHPSAO\Model\Arrays\ArrayStringFloat;

/**
 * Class ArrayStringFloatTest
 * @package PHPSAO\Tests\Model\Arrays
 */
class ArrayStringFloatTest extends ReflectionTestClass
{
    private array $array = [];
    private array $arrayPrepended = [];

    protected function setUp(): void
    {
        $this->array = ['index0' => 1.1, 'index1' => 2.2];
        $this->arrayPrepended = ['index2' => 3.3] + $this->array;
    }

    public function testToString()
    {
        static::assertSame(\json_encode($this->array), (new ArrayStringFloat($this->array))->__toString());
    }

    public function testInterfaces()
    {
        parent::validateInterfaces(ArrayStringFloat::class);
    }

    public function testAddByIndex()
    {
        $instance = new ArrayStringFloat;
        $instance->addByIndex('0', 1.1);
        static::assertSame(1.1, $instance->get('0'));
        $instance->addByIndex('1', 2.2);
        static::assertSame(2.2, $instance->get('1'));
    }

    public function testAddCollection()
    {
        $instance1 = new ArrayStringFloat($this->array);
        $instance2 = new ArrayStringFloat;
        $instance2->addCollection($instance1);
        static::assertSame($instance1->getArray(), $instance2->getArray());
    }

    public function testAddRange()
    {
        $instance = new ArrayStringFloat;
        $instance->addRange($this->array);
        static::assertSame($this->array, $instance->getArray());
    }

    public function testChunk()
    {
        $instances = (new ArrayStringFloat($this->array))->chunk(1, ArrayStringFloat::ARRAY_PRESERVE_KEYS);
        static::assertCount(2, $instances);
    }

    public function testClear()
    {
        $instance = new ArrayStringFloat($this->array);
        $instance->clear();
        static::assertSame([], $instance->getArray());
    }

    public function testContains()
    {
        static::assertTrue((new ArrayStringFloat($this->array))->contains(1.1));
        static::assertFalse((new ArrayStringFloat($this->array))->contains(3.3));
    }

    public function testContainsKey()
    {
        static::assertTrue((new ArrayStringFloat($this->array))->containsKey('index1'));
        static::assertFalse((new ArrayStringFloat($this->array))->containsKey('index3'));
    }

    public function testCopy()
    {
        $instances = (new ArrayStringFloat($this->array));
        static::assertEquals($instances, $instances->copy());
    }

    public function testCount()
    {
        static::assertSame(2, (new ArrayStringFloat($this->array))->count());
        static::assertSame(2, (new ArrayStringFloat($this->array))->size());
    }

    public function testGetCurrent()
    {
        static::assertSame(\current($this->array), (new ArrayStringFloat($this->array))->getCurrent());
    }

    public function testFilter()
    {
        static::assertInstanceOf(
            ArrayStringFloat::class,
            (new ArrayStringFloat($this->array))->filter(static function(float $item): bool
            {
                return \is_float($item);
            })
        );
    }

    public function testGet()
    {
        $instance = new ArrayStringFloat($this->array);
        static::assertSame(1.1, $instance->get('index0'));

        $this->expectException(OutOfBoundsException::class);
        $instance->get('index-1');
    }

    public function testCurrent()
    {
        $instance = new ArrayStringFloat($this->array);
        static::assertSame(1.1, $instance->getCurrent());
        $instance->next();
        static::assertSame(2.2, $instance->getCurrent());
    }

    public function testItems()
    {
        static::assertIsArray((new ArrayStringFloat($this->array))->getArray());
    }

    public function testGetIterator()
    {
        static::assertInstanceOf(ArrayIterator::class, (new ArrayStringFloat($this->array))->getIterator());
    }

    public function testGetKey()
    {
        $instance = new ArrayStringFloat($this->array);
        static::assertSame('index0', $instance->getKey());
        $instance->next();
        static::assertSame('index1', $instance->getKey());
    }

    public function testGetKeys()
    {
        static::assertSame(
            \array_keys($this->array, ArrayStringFloat::ARRAY_KEYS_SEARCH_VALUE, ArrayStringFloat::ARRAY_KEYS_STRICT),
            (new ArrayStringFloat($this->array))->getKeys()
        );
    }

    public function testGetValues()
    {
        static::assertSame(\array_values($this->array), (new ArrayStringFloat($this->array))->getValues());
    }

    public function testIsEmpty()
    {
        static::assertTrue((new ArrayStringFloat)->isEmpty());
    }

    public function testLast()
    {
        $instance = new ArrayStringFloat($this->array);
        $instance->last();
        static::assertSame(\end($this->array), $instance->getCurrent());
    }

    public function testMap()
    {
        static::assertInstanceOf(
            ArrayStringFloat::class,
            (new ArrayStringFloat($this->array))->map(static function(float $item): float
            {
                return $item;
            })
        );
    }

    public function testNext()
    {
        $instance = new ArrayStringFloat($this->array);
        $instance->next();
        static::assertSame(\next($this->array), $instance->getCurrent());
    }

    public function testPrependByIndex()
    {
        $array = $this->arrayPrepended;
        \reset($array);

        $instance = new ArrayStringFloat($this->array);
        $instance->prependByIndex('index2', 3.3);
        $instance->reset();
        static::assertSame(\current($array), $instance->getCurrent());
    }

    public function testPrevious()
    {
        $instance = new ArrayStringFloat($this->array);
        $instance->last();
        $instance->previous();
        static::assertSame(\current($this->array), $instance->getCurrent());
    }

    public function testReset()
    {
        $instance = new ArrayStringFloat($this->array);
        $instance->last();
        $instance->reset();
        static::assertSame(\current($this->array), $instance->getCurrent());
    }

    public function testSize()
    {
        static::assertSame(\count($this->array), (new ArrayStringFloat($this->array))->size());
    }

    public function testSlice()
    {
        $instances = (new ArrayStringFloat($this->array))->slice(1, 1, ArrayStringFloat::ARRAY_PRESERVE_KEYS);
        static::assertInstanceOf(ArrayStringFloat::class, $instances);
        static::assertCount(1, $instances);
    }
}
