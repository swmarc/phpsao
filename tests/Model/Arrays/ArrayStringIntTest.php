<?php
declare(strict_types = 1);

namespace PHPSAO\Tests\Model\Arrays;

use ArrayIterator;
use OutOfBoundsException;
use PHPSAO\Tests\Core\ReflectionTestClass;
use PHPSAO\Model\Arrays\ArrayStringInt;

/**
 * Class ArrayStringIntTest
 * @package PHPSAO\Tests\Model\Arrays
 */
class ArrayStringIntTest extends ReflectionTestClass
{
    private array $array = [];
    private array $arrayPrepended = [];
    private array $arrayUnordered = [];

    protected function setUp(): void
    {
        $this->array = ['index0' => 1, 'index1' => 2];
        $this->arrayPrepended = ['index2' => 3] + $this->array;
        $this->arrayUnordered = ['index1' => 2, 'index0' => 1];
    }

    public function testToString()
    {
        static::assertSame(\json_encode($this->array), (new ArrayStringInt($this->array))->__toString());
    }

    public function testInterfaces()
    {
        parent::validateInterfaces(ArrayStringInt::class);
    }

    public function testAddByIndex()
    {
        $instance = new ArrayStringInt;
        $instance->addByIndex('0', 1);
        static::assertSame(1, $instance->get('0'));
        $instance->addByIndex('1', 2);
        static::assertSame(2, $instance->get('1'));
    }

    public function testAddCollection()
    {
        $instance1 = new ArrayStringInt($this->array);
        $instance2 = new ArrayStringInt;
        $instance2->addCollection($instance1);
        static::assertSame($instance1->getArray(), $instance2->getArray());
    }

    public function testAddRange()
    {
        $instance = new ArrayStringInt;
        $instance->addRange($this->array);
        static::assertSame($this->array, $instance->getArray());
    }

    public function testChunk()
    {
        $instances = (new ArrayStringInt($this->array))->chunk(1, ArrayStringInt::ARRAY_PRESERVE_KEYS);
        static::assertCount(2, $instances);
    }

    public function testClear()
    {
        $instance = new ArrayStringInt($this->array);
        $instance->clear();
        static::assertSame([], $instance->getArray());
    }

    public function testCopy()
    {
        $instance = (new ArrayStringInt($this->array));
        static::assertEquals($instance, $instance->copy());
    }

    public function testContains()
    {
        static::assertTrue((new ArrayStringInt($this->array))->contains(1));
        static::assertFalse((new ArrayStringInt($this->array))->contains(3));
    }

    public function testContainsKey()
    {
        static::assertTrue((new ArrayStringInt($this->array))->containsKey('index1'));
        static::assertFalse((new ArrayStringInt($this->array))->containsKey('index3'));
    }

    public function testCount()
    {
        static::assertSame(2, (new ArrayStringInt($this->array))->count());
        static::assertSame(2, (new ArrayStringInt($this->array))->size());
    }

    public function testGetCurrent()
    {
        static::assertSame(\current($this->array), (new ArrayStringInt($this->array))->getCurrent());
    }

    public function testFilter()
    {
        static::assertInstanceOf(
            ArrayStringInt::class,
            (new ArrayStringInt($this->array))->filter(static function(int $item): bool
            {
                return \is_int($item);
            })
        );
    }

    public function testGet()
    {
        $instance = new ArrayStringInt($this->array);
        static::assertSame(1, $instance->get('index0'));

        $this->expectException(OutOfBoundsException::class);
        $instance->get('index-1');
    }

    public function testCurrent()
    {
        $instance = new ArrayStringInt($this->array);
        static::assertSame(1, $instance->getCurrent());
        $instance->next();
        static::assertSame(2, $instance->getCurrent());
    }

    public function testItems()
    {
        static::assertIsArray((new ArrayStringInt($this->array))->getArray());
    }

    public function testGetIterator()
    {
        static::assertInstanceOf(ArrayIterator::class, (new ArrayStringInt($this->array))->getIterator());
    }

    public function testGetKey()
    {
        $instance = new ArrayStringInt($this->array);
        static::assertSame('index0', $instance->getKey());
        $instance->next();
        static::assertSame('index1', $instance->getKey());
    }

    public function testGetKeys()
    {
        static::assertSame(
            \array_keys($this->array, ArrayStringInt::ARRAY_KEYS_SEARCH_VALUE, ArrayStringInt::ARRAY_KEYS_STRICT),
            (new ArrayStringInt($this->array))->getKeys()
        );
    }

    public function testGetValues()
    {
        static::assertSame(\array_values($this->array), (new ArrayStringInt($this->array))->getValues());
    }

    public function testIsEmpty()
    {
        static::assertTrue((new ArrayStringInt)->isEmpty());
    }

    public function testLast()
    {
        $instance = new ArrayStringInt($this->array);
        $instance->last();
        static::assertSame(\end($this->array), $instance->getCurrent());
    }

    public function testMap()
    {
        static::assertInstanceOf(
            ArrayStringInt::class,
            (new ArrayStringInt($this->array))->map(static function(int $item): int
            {
                return $item;
            })
        );
    }

    public function testNext()
    {
        $instance = new ArrayStringInt($this->array);
        $instance->next();
        static::assertSame(\next($this->array), $instance->getCurrent());
    }

    public function testPrependByIndex()
    {
        $array = $this->arrayPrepended;
        \reset($array);

        $instance = new ArrayStringInt($this->array);
        $instance->prependByIndex('index2', 3);
        $instance->reset();
        static::assertSame(\current($array), $instance->getCurrent());
    }

    public function testPrevious()
    {
        $instance = new ArrayStringInt($this->array);
        $instance->last();
        $instance->previous();
        static::assertSame(\current($this->array), $instance->getCurrent());
    }

    public function testReset()
    {
        $instance = new ArrayStringInt($this->array);
        $instance->last();
        $instance->reset();
        static::assertSame(\current($this->array), $instance->getCurrent());
    }

    public function testSize()
    {
        static::assertSame(\count($this->array), (new ArrayStringInt($this->array))->size());
    }

    public function testSlice()
    {
        $instances = (new ArrayStringInt($this->array))->slice(1, 1, ArrayStringInt::ARRAY_PRESERVE_KEYS);
        static::assertInstanceOf(ArrayStringInt::class, $instances);
        static::assertCount(1, $instances);
    }
}
