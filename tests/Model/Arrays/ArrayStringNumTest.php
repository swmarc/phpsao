<?php
declare(strict_types=1);

namespace PHPSAO\Tests\Model\Arrays;

use ArrayIterator;
use OutOfBoundsException;
use PHPSAO\Model\Arrays\ArrayStringNum;
use PHPSAO\Model\Arrays\Exceptions\TypeMismatchException;
use PHPSAO\Tests\Core\ReflectionTestClass;

/**
 * Class ArrayStringNumTest
 * @package PHPSAO\Tests\Model\Arrays
 */
class ArrayStringNumTest extends ReflectionTestClass
{
    private array $arrayValid = [];
    private array $arrayInvalid = [];
    private array $arrayPrepended = [];
    private array $arrayUnordered = [];

    protected function setUp(): void
    {
        $this->arrayValid = [
            'index1' => 1,
            'index2' => 2.3,
        ];

        $this->arrayInvalid = [
            'index0' => '1',
            'index1' => 2.3,
        ];

        $this->arrayPrepended = ['index2' => 3] + $this->arrayValid;
        $this->arrayUnordered = ['index1' => 2.3, 'index0' => 1];
    }

    public function testToString()
    {
        static::assertSame(\json_encode($this->arrayValid), (new ArrayStringNum($this->arrayValid))->__toString());
    }

    public function testInterfaces()
    {
        parent::validateInterfaces(ArrayStringNum::class);
    }

    public function testConstructor()
    {
        static::assertInstanceOf(ArrayStringNum::class, new ArrayStringNum($this->arrayValid));

        $this->expectException(TypeMismatchException::class);
        new ArrayStringNum($this->arrayInvalid);
    }

    public function testAddByIndex()
    {
        $instance = new ArrayStringNum;
        $instance->addByIndex('index1', 1);
        static::assertSame(1, $instance->get('index1'));

        $instance = new ArrayStringNum;
        $instance->addByIndex('index1', 2.3);
        static::assertSame(2.3, $instance->get('index1'));

        $this->expectException(TypeMismatchException::class);
        (new ArrayStringNum)->addByIndex('index2', true);
    }

    public function testAddCollection()
    {
        $instance1 = new ArrayStringNum($this->arrayValid);
        $instance2 = new ArrayStringNum;
        $instance2->addCollection($instance1);
        static::assertSame($instance1->getArray(), $instance2->getArray());

        $this->expectException(TypeMismatchException::class);
        (new ArrayStringNum)->addCollection(new ArrayStringNum(['index0' => true]));
    }

    public function testAddRange()
    {
        $instance = new ArrayStringNum;
        $instance->addRange($this->arrayValid);
        static::assertSame($this->arrayValid, $instance->getArray());

        $this->expectException(TypeMismatchException::class);
        (new ArrayStringNum)->addRange(['index0' => true]);
    }

    public function testChunk()
    {
        $instances = (new ArrayStringNum($this->arrayValid))->chunk(1, ArrayStringNum::ARRAY_PRESERVE_KEYS);
        static::assertCount(2, $instances);
    }

    public function testClear()
    {
        $instance = new ArrayStringNum($this->arrayValid);
        $instance->clear();
        static::assertSame([], $instance->getArray());
    }

    public function testContains()
    {
        static::assertTrue((new ArrayStringNum($this->arrayValid))->contains(1));
        static::assertFalse((new ArrayStringNum($this->arrayValid))->contains(3));

        $this->expectException(TypeMismatchException::class);
        static::assertFalse((new ArrayStringNum($this->arrayValid))->contains(true));
    }

    public function testContainsKey()
    {
        static::assertTrue((new ArrayStringNum($this->arrayValid))->containsKey('index1'));
        static::assertFalse((new ArrayStringNum($this->arrayValid))->containsKey('index3'));
    }

    public function testCopy()
    {
        $instance = (new ArrayStringNum($this->arrayValid));
        static::assertEquals($instance, $instance->copy());
    }

    public function testCount()
    {
        static::assertSame(2, (new ArrayStringNum($this->arrayValid))->count());
        static::assertSame(2, (new ArrayStringNum($this->arrayValid))->size());
    }

    public function testGetCurrent()
    {
        static::assertSame(\current($this->arrayValid), (new ArrayStringNum($this->arrayValid))->getCurrent());
    }

    public function testFilter()
    {
        static::assertInstanceOf(
            ArrayStringNum::class,
            (new ArrayStringNum($this->arrayValid))->filter(static function($item): bool
            {
                return \is_numeric($item);
            })
        );
    }

    public function testGet()
    {
        $instance = new ArrayStringNum($this->arrayValid);
        static::assertSame(1, $instance->get('index1'));

        $this->expectException(OutOfBoundsException::class);
        $instance->get('index-1');
    }

    public function testCurrent()
    {
        $instance = new ArrayStringNum($this->arrayValid);
        static::assertSame(1, $instance->getCurrent());
        $instance->next();
        static::assertSame(2.3, $instance->getCurrent());
    }

    public function testItems()
    {
        static::assertIsArray((new ArrayStringNum($this->arrayValid))->getArray());
    }

    public function testGetIterator()
    {
        static::assertInstanceOf(ArrayIterator::class, (new ArrayStringNum($this->arrayValid))->getIterator());
    }

    public function testGetKey()
    {
        $instance = new ArrayStringNum($this->arrayValid);
        static::assertSame('index1', $instance->getKey());
        $instance->next();
        static::assertSame('index2', $instance->getKey());
    }

    public function testGetKeys()
    {
        static::assertSame(
            \array_keys($this->arrayValid, ArrayStringNum::ARRAY_KEYS_SEARCH_VALUE, ArrayStringNum::ARRAY_KEYS_STRICT),
            (new ArrayStringNum($this->arrayValid))->getKeys()
        );
    }

    public function testGetValues()
    {
        static::assertSame(\array_values($this->arrayValid), (new ArrayStringNum($this->arrayValid))->getValues());
    }

    public function testIsEmpty()
    {
        static::assertTrue((new ArrayStringNum)->isEmpty());
    }

    public function testLast()
    {
        $instance = new ArrayStringNum($this->arrayValid);
        $instance->last();
        static::assertSame(\end($this->arrayValid), $instance->getCurrent());
    }

    public function testMap()
    {
        static::assertInstanceOf(
            ArrayStringNum::class,
            (new ArrayStringNum($this->arrayValid))->map(static function($item)
            {
                return $item;
            })
        );
    }

    public function testNext()
    {
        $instance = new ArrayStringNum($this->arrayValid);
        $instance->next();
        static::assertSame(\next($this->arrayValid), $instance->getCurrent());
    }

    public function testPrependByIndex()
    {
        $array = $this->arrayPrepended;
        \reset($array);

        $instance = new ArrayStringNum($this->arrayValid);
        $instance->prependByIndex('index2', 3);
        $instance->reset();
        static::assertSame(\current($array), $instance->getCurrent());
    }

    public function testPrevious()
    {

        $instance = new ArrayStringNum($this->arrayValid);
        $instance->last();
        $instance->previous();
        static::assertSame(\current($this->arrayValid), $instance->getCurrent());
    }

    public function testReset()
    {
        $instance = new ArrayStringNum($this->arrayValid);
        $instance->last();
        $instance->reset();
        static::assertSame(\current($this->arrayValid), $instance->getCurrent());
    }

    public function testSize()
    {
        static::assertSame(\count($this->arrayValid), (new ArrayStringNum($this->arrayValid))->size());
    }

    public function testSlice()
    {
        $instances = (new ArrayStringNum($this->arrayValid))->slice(1, 1, ArrayStringNum::ARRAY_PRESERVE_KEYS);
        static::assertInstanceOf(ArrayStringNum::class, $instances);
        static::assertCount(1, $instances);
    }
}
