<?php
declare(strict_types=1);

namespace PHPSAO\Tests\Model\Arrays;

use ArrayIterator;
use OutOfBoundsException;
use PHPSAO\Model\Arrays\ArrayStringObject;
use PHPSAO\Model\Arrays\Exceptions\TypeMismatchException;
use PHPSAO\Tests\Core\ReflectionTestClass;
use PHPSAO\Tests\Model\Arrays\Entity\InvalidType;
use PHPSAO\Tests\Model\Arrays\Entity\ValidType;

/**
 * Class ArrayStringObjectTest
 * @package PHPSAO\Tests\Model\Arrays
 */
class ArrayStringObjectTest extends ReflectionTestClass
{
    private array $arrayValid = [];
    private array $arrayInvalid = [];

    protected function setUp(): void
    {
        $this->arrayValid = [
            '01' => new ValidType,
            '02' => new ValidType,
        ];

        $this->arrayInvalid = [
            '01' => new ValidType,
            '02' => new InvalidType,
        ];
    }

    public function testToString()
    {
        static::assertSame(\json_encode($this->arrayValid), (new ArrayStringObject($this->arrayValid))->__toString());
    }

    public function testInterfaces()
    {
        parent::validateInterfaces(ArrayStringObject::class);
    }

    public function testConstructor()
    {
        static::assertInstanceOf(ArrayStringObject::class, new ArrayStringObject($this->arrayValid));

        $this->expectException(TypeMismatchException::class);
        new ArrayStringObject($this->arrayInvalid);
    }

    public function testAddByIndex()
    {
        $instance = new ArrayStringObject;
        $instance->addByIndex('01', new ValidType);
        static::assertInstanceOf(ValidType::class, $instance->get('01'));

        $this->expectException(TypeMismatchException::class);
        $instance->addByIndex('02', new InvalidType);
    }

    public function testAddCollection()
    {
        $instance1 = new ArrayStringObject($this->arrayValid);
        $instance2 = new ArrayStringObject;
        $instance2->addCollection($instance1);
        static::assertSame($instance1->getArray(), $instance2->getArray());

        $this->expectException(TypeMismatchException::class);
        (new ArrayStringObject)->addCollection(new ArrayStringObject($this->arrayInvalid));
    }

    public function testAddRange()
    {
        $instance = new ArrayStringObject;
        $instance->addRange($this->arrayValid);
        static::assertSame($this->arrayValid, $instance->getArray());

        $this->expectException(TypeMismatchException::class);
        (new ArrayStringObject)->addRange($this->arrayInvalid);
    }

    public function testChunk()
    {
        $instances = (new ArrayStringObject($this->arrayValid))->chunk(1, ArrayStringObject::ARRAY_PRESERVE_KEYS);
        static::assertCount(2, $instances);
    }

    public function testClear()
    {
        $instance = new ArrayStringObject($this->arrayValid);
        $instance->clear();
        static::assertSame([], $instance->getArray());
    }

    public function testContains()
    {
        $object = new ValidType;
        $instance = new ArrayStringObject(['01' => $object]);
        static::assertTrue($instance->contains($object));
        static::assertFalse($instance->contains(new ValidType));

        $this->expectException(TypeMismatchException::class);
        $instance->contains(new InvalidType);
    }

    public function testContainsKey()
    {
        $instance = (new ArrayStringObject($this->arrayValid));
        static::assertTrue($instance->containsKey('01'));
        static::assertFalse($instance->containsKey('03'));
    }

    public function testCopy()
    {
        $instance = (new ArrayStringObject($this->arrayValid));
        static::assertEquals($instance, $instance->copy());
    }

    public function testCount()
    {
        static::assertSame(2, (new ArrayStringObject($this->arrayValid))->count());
        static::assertSame(2, (new ArrayStringObject($this->arrayValid))->size());
    }

    public function testGetCurrent()
    {
        static::assertSame(\current($this->arrayValid), (new ArrayStringObject($this->arrayValid))->getCurrent());
    }

    public function testFilter()
    {
        static::assertInstanceOf(
            ArrayStringObject::class,
            (new ArrayStringObject($this->arrayValid))->filter(static function (object $item): bool {
                return \is_object($item);
            })
        );
    }

    public function testGet()
    {
        $object = new ValidType;
        $instance = new ArrayStringObject(['01' => $object]);
        static::assertSame($object, $instance->get('01'));

        $this->expectException(OutOfBoundsException::class);
        $instance->get('-01');
    }

    public function testCurrent()
    {
        $object1 = new ValidType;
        $object2 = new ValidType;
        $instance = new ArrayStringObject(['01' => $object1, '02' => $object2]);
        static::assertSame($object1, $instance->getCurrent());
        $instance->next();
        static::assertSame($object2, $instance->getCurrent());
    }

    public function testItems()
    {
        static::assertIsArray((new ArrayStringObject($this->arrayValid))->getArray());
    }

    public function testGetIterator()
    {
        static::assertInstanceOf(ArrayIterator::class, (new ArrayStringObject($this->arrayValid))->getIterator());
    }

    public function testGetKey()
    {
        $instance = new ArrayStringObject($this->arrayValid);
        static::assertSame('01', $instance->getKey());
        $instance->next();
        static::assertSame('02', $instance->getKey());
    }

    public function testGetKeys()
    {
        static::assertSame(
            \array_keys(
                $this->arrayValid,
                ArrayStringObject::ARRAY_KEYS_SEARCH_VALUE,
                ArrayStringObject::ARRAY_KEYS_STRICT
            ),
            (new ArrayStringObject($this->arrayValid))->getKeys()
        );
    }

    public function testGetValues()
    {
        static::assertSame(\array_values($this->arrayValid), (new ArrayStringObject($this->arrayValid))->getValues());
    }

    public function testIsEmpty()
    {
        static::assertTrue((new ArrayStringObject)->isEmpty());
    }

    public function testLast()
    {
        $instance = new ArrayStringObject($this->arrayValid);
        $instance->last();
        static::assertSame(\end($this->arrayValid), $instance->getCurrent());
    }

    public function testMap()
    {
        static::assertInstanceOf(
            ArrayStringObject::class,
            (new ArrayStringObject($this->arrayValid))->map(static function (object $item): object {
                return $item;
            })
        );
    }

    public function testNext()
    {
        $instance = new ArrayStringObject($this->arrayValid);
        $instance->next();
        static::assertSame(\next($this->arrayValid), $instance->getCurrent());
    }

    public function testPrependByIndex()
    {
        $instance = new ArrayStringObject($this->arrayValid);
        $instance->prependByIndex('03', new ValidType);
        $instance->reset();
        static::assertInstanceOf(ValidType::class, $instance->getCurrent());

        $this->expectException(TypeMismatchException::class);
        (new ArrayStringObject($this->arrayValid))->prependByIndex('03', new InvalidType);
    }

    public function testPrevious()
    {
        $instance = new ArrayStringObject($this->arrayValid);
        $instance->last();
        $instance->previous();
        static::assertSame(\current($this->arrayValid), $instance->getCurrent());
    }

    public function testReset()
    {
        $instance = new ArrayStringObject($this->arrayValid);
        $instance->last();
        $instance->reset();
        static::assertSame(\current($this->arrayValid), $instance->getCurrent());
    }

    public function testSize()
    {
        static::assertSame(\count($this->arrayValid), (new ArrayStringObject($this->arrayValid))->size());
    }

    public function testSlice()
    {
        $instances = (new ArrayStringObject($this->arrayValid))->slice(1, 1, ArrayStringObject::ARRAY_PRESERVE_KEYS);
        static::assertInstanceOf(ArrayStringObject::class, $instances);
        static::assertCount(1, $instances);
    }
}
