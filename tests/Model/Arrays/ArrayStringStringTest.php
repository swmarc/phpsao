<?php
declare(strict_types = 1);

namespace PHPSAO\Tests\Model\Arrays;

use ArrayIterator;
use OutOfBoundsException;
use PHPSAO\Tests\Core\ReflectionTestClass;
use PHPSAO\Model\Arrays\ArrayStringString;

/**
 * Class ArrayStringStringTest
 * @package PHPSAO\Tests\Model\Arrays
 */
class ArrayStringStringTest extends ReflectionTestClass
{
    private array $array = [];
    private array $arrayPrepended = [];

    protected function setUp(): void
    {
        $this->array = ['index0' => '1', 'index1' => '2'];
        $this->arrayPrepended = ['index2' => '3'] + $this->array;
    }

    public function testToString()
    {
        static::assertSame(\json_encode($this->array), (new ArrayStringString($this->array))->__toString());
    }

    public function testInterfaces()
    {
        parent::validateInterfaces(ArrayStringString::class);
    }

    public function testAddByIndex()
    {
        $instance = new ArrayStringString;
        $instance->addByIndex('index0', '1');
        static::assertSame('1', $instance->get('index0'));
    }

    public function testAddCollection()
    {
        $instance1 = new ArrayStringString($this->array);
        $instance2 = new ArrayStringString;
        $instance2->addCollection($instance1);
        static::assertSame($instance1->getArray(), $instance2->getArray());
    }

    public function testAddRange()
    {
        $instance = new ArrayStringString;
        $instance->addRange($this->array);
        static::assertSame($this->array, $instance->getArray());
    }

    public function testChunk()
    {
        $instances = (new ArrayStringString($this->array))->chunk(1, ArrayStringString::ARRAY_PRESERVE_KEYS);
        static::assertCount(2, $instances);
    }

    public function testClear()
    {
        $instance = new ArrayStringString($this->array);
        $instance->clear();
        static::assertSame([], $instance->getArray());
    }

    public function testCopy()
    {
        $instance = (new ArrayStringString($this->array));
        static::assertEquals($instance, $instance->copy());
    }

    public function testContains()
    {
        static::assertTrue((new ArrayStringString($this->array))->contains('1'));
        static::assertFalse((new ArrayStringString($this->array))->contains('3'));
    }

    public function testContainsKey()
    {
        static::assertTrue((new ArrayStringString($this->array))->containsKey('index1'));
        static::assertFalse((new ArrayStringString($this->array))->containsKey('index3'));
    }

    public function testCount()
    {
        static::assertSame(2, (new ArrayStringString($this->array))->count());
        static::assertSame(2, (new ArrayStringString($this->array))->size());
    }

    public function testGetCurrent()
    {
        static::assertSame(\current($this->array), (new ArrayStringString($this->array))->getCurrent());
    }

    public function testFilter()
    {
        static::assertInstanceOf(
            ArrayStringString::class,
            (new ArrayStringString($this->array))->filter(static function(string $item): bool
            {
                return \is_string($item);
            })
        );
    }

    public function testGet()
    {
        $instance = new ArrayStringString($this->array);
        static::assertSame('1', $instance->get('index0'));

        $this->expectException(OutOfBoundsException::class);
        $instance->get('index-1');
    }

    public function testCurrent()
    {
        $instance = new ArrayStringString($this->array);
        static::assertSame('1', $instance->getCurrent());
        $instance->next();
        static::assertSame('2', $instance->getCurrent());
    }

    public function testItems()
    {
        static::assertIsArray((new ArrayStringString($this->array))->getArray());
    }

    public function testGetIterator()
    {
        static::assertInstanceOf(ArrayIterator::class, (new ArrayStringString($this->array))->getIterator());
    }

    public function testGetKey()
    {
        $instance = new ArrayStringString($this->array);
        static::assertSame('index0', $instance->getKey());
        $instance->next();
        static::assertSame('index1', $instance->getKey());
    }

    public function testGetKeys()
    {
        static::assertSame(
            \array_keys($this->array, ArrayStringString::ARRAY_KEYS_SEARCH_VALUE, ArrayStringString::ARRAY_KEYS_STRICT),
            (new ArrayStringString($this->array))->getKeys()
        );
    }

    public function testGetValues()
    {
        static::assertSame(\array_values($this->array), (new ArrayStringString($this->array))->getValues());
    }

    public function testIsEmpty()
    {
        static::assertTrue((new ArrayStringString)->isEmpty());
    }

    public function testLast()
    {
        $instance = new ArrayStringString($this->array);
        $instance->last();
        static::assertSame(\end($this->array), $instance->getCurrent());
    }

    public function testMap()
    {
        static::assertInstanceOf(
            ArrayStringString::class,
            (new ArrayStringString($this->array))->map(static function(string $item): string
            {
                return $item;
            })
        );
    }

    public function testNext()
    {
        $instance = new ArrayStringString($this->array);
        $instance->next();
        static::assertSame(\next($this->array), $instance->getCurrent());
    }

    public function testPrependByIndex()
    {
        $array = $this->arrayPrepended;
        \reset($array);

        $instance = new ArrayStringString($this->array);
        $instance->prependByIndex('index2', '3');
        $instance->reset();
        static::assertSame(\current($array), $instance->getCurrent());
    }

    public function testPrevious()
    {
        $instance = new ArrayStringString($this->array);
        $instance->last();
        $instance->previous();
        static::assertSame(\current($this->array), $instance->getCurrent());
    }

    public function testReset()
    {
        $instance = new ArrayStringString($this->array);
        $instance->last();
        $instance->reset();
        static::assertSame(\current($this->array), $instance->getCurrent());
    }

    public function testSize()
    {
        static::assertSame(\count($this->array), (new ArrayStringString($this->array))->size());
    }

    public function testSlice()
    {
        $instances = (new ArrayStringString($this->array))->slice(1, 1, ArrayStringString::ARRAY_PRESERVE_KEYS);
        static::assertInstanceOf(ArrayStringString::class, $instances);
        static::assertCount(1, $instances);
    }
}
