<?php
declare(strict_types=1);

namespace PHPSAO\Tests\Model\Arrays;

use PHPSAO\Model\Arrays\ArrayIntBool;
use PHPSAO\Model\Arrays\ArrayIntFloat;
use PHPSAO\Model\Arrays\ArrayIntInt;
use PHPSAO\Model\Arrays\ArrayIntNum;
use PHPSAO\Model\Arrays\ArrayIntObject;
use PHPSAO\Model\Arrays\ArrayIntString;
use PHPSAO\Model\Arrays\ArrayStringBool;
use PHPSAO\Model\Arrays\ArrayStringFloat;
use PHPSAO\Model\Arrays\ArrayStringInt;
use PHPSAO\Model\Arrays\ArrayStringNum;
use PHPSAO\Model\Arrays\ArrayStringObject;
use PHPSAO\Model\Arrays\ArrayStringString;
use PHPSAO\Model\Arrays\Generic;
use PHPSAO\Model\Arrays\Exceptions\TypeMismatchException;
use PHPSAO\Tests\Core\ReflectionTestClass;
use PHPSAO\Tests\Model\Arrays\Entity\ValidType;

/**
 * Class GenericTest
 * @package PHPSAO\Tests\Model\Arrays
 */
class GenericTest extends ReflectionTestClass
{
    public function testInterfaces()
    {
        parent::validateInterfaces(Generic::class);
    }

    public function testTypeMismatchKey()
    {
        $this->expectException(TypeMismatchException::class);
        Generic::create('object', null);
    }

    public function testTypeMismatchNullValue()
    {
        $this->expectException(TypeMismatchException::class);
        Generic::create('int', null);
    }

    public function testInvoke()
    {
        $instance = new Generic;
        static::assertInstanceOf(ArrayIntObject::class, $instance('int', new ValidType));
    }

    public function testIntBool()
    {
        static::assertInstanceOf(ArrayIntBool::class, Generic::create('int', 'bool'));
    }

    public function testIntFloat()
    {
        static::assertInstanceOf(ArrayIntFloat::class, Generic::create('int', 'float'));
    }

    public function testIntInt()
    {
        static::assertInstanceOf(ArrayIntInt::class, Generic::create('int', 'int'));
    }

    public function testIntString()
    {
        static::assertInstanceOf(ArrayIntString::class, Generic::create('int', 'string'));
    }

    public function testStringBool()
    {
        static::assertInstanceOf(ArrayStringBool::class, Generic::create('string', 'bool'));
    }

    public function testStringFloat()
    {
        static::assertInstanceOf(ArrayStringFloat::class, Generic::create('string', 'float'));
    }

    public function testStringInt()
    {
        static::assertInstanceOf(ArrayStringInt::class, Generic::create('string', 'int'));
    }

    public function testStringString()
    {
        static::assertInstanceOf(ArrayStringString::class, Generic::create('string', 'string'));
    }

    public function testIntObject()
    {
        static::assertInstanceOf(ArrayIntObject::class, Generic::create('int', new ValidType));
    }

    public function testStringObject()
    {
        static::assertInstanceOf(ArrayStringObject::class, Generic::create('string', new ValidType));
    }

    public function testIntNum()
    {
        static::assertInstanceOf(ArrayIntNum::class, Generic::create('int', 'num'));
    }

    public function testStringNum()
    {
        static::assertInstanceOf(ArrayStringNum::class, Generic::create('string', 'num'));
    }
}
